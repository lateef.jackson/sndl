// There is a lot of copy pasta from https://github.com/yuin/gopher-lua/blob/master/oslib.go in this library
package sndl

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	lua "github.com/yuin/gopher-lua"
)

// Copy pasta:https://github.com/yuin/gopher-lua/blob/master/oslib.go#L10-L54
var startedAt time.Time

func init() {
	startedAt = time.Now()
}
func getIntField(_ *lua.LState, tb *lua.LTable, key string, v int) int {
	ret := tb.RawGetString(key)

	switch lv := ret.(type) {
	case lua.LNumber:
		return int(lv)
	case lua.LString:
		slv := string(lv)
		slv = strings.TrimLeft(slv, " ")
		if strings.HasPrefix(slv, "0") && !strings.HasPrefix(slv, "0x") && !strings.HasPrefix(slv, "0X") {
			// Standard lua interpreter only support decimal and hexadecimal
			slv = strings.TrimLeft(slv, "0")
			if slv == "" {
				return 0
			}
		}
		if num, err := parseNumber(slv); err == nil {
			return int(num)
		}
	default:
		return v
	}

	return v
}

func getBoolField(_ *lua.LState, tb *lua.LTable, key string, v bool) bool {
	ret := tb.RawGetString(key)
	if lb, ok := ret.(lua.LBool); ok {
		return bool(lb)
	}
	return v
}
func OpenOs(L *lua.LState) int {
	osmod := L.RegisterModule(lua.OsLibName, osFuncs)
	L.Push(osmod)
	return 1
}

var osFuncs = map[string]lua.LGFunction{
	"clock":    osClock,
	"difftime": osDiffTime,
	//"execute":   osExecute,
	//"exit":      osExit,
	"date":   osDate,
	"getenv": osGetEnv,
	//	"remove":    osRemove,
	//	"rename":    osRename,
	"setenv": osSetEnv,
	//	"setlocale": osSetLocale,
	"time": osTime,
	// "tmpname":   osTmpname,
}

// https://github.com/yuin/gopher-lua/blob/master/oslib.go#L71C1-L79C2
func osClock(L *lua.LState) int {
	L.Push(lua.LNumber(float64(time.Since(startedAt)) / float64(time.Second)))
	return 1
}

func osDiffTime(L *lua.LState) int {
	L.Push(lua.LNumber(L.CheckInt64(1) - L.CheckInt64(2)))
	return 1
}

// Copy pasta: https://github.com/yuin/gopher-lua/blob/master/oslib.go#L10-L54
func osDate(L *lua.LState) int {
	t := time.Now()
	isUTC := false
	cfmt := "%c"
	if L.GetTop() >= 1 {
		cfmt = L.CheckString(1)
		if strings.HasPrefix(cfmt, "!") {
			cfmt = strings.TrimLeft(cfmt, "!")
			isUTC = true
		}
		if L.GetTop() >= 2 {
			t = time.Unix(L.CheckInt64(2), 0)
		}
		if isUTC {
			t = t.UTC()
		}
		if strings.HasPrefix(cfmt, "*t") {
			ret := L.NewTable()
			ret.RawSetString("year", lua.LNumber(t.Year()))
			ret.RawSetString("month", lua.LNumber(t.Month()))
			ret.RawSetString("day", lua.LNumber(t.Day()))
			ret.RawSetString("hour", lua.LNumber(t.Hour()))
			ret.RawSetString("min", lua.LNumber(t.Minute()))
			ret.RawSetString("sec", lua.LNumber(t.Second()))
			ret.RawSetString("wday", lua.LNumber(t.Weekday()+1))
			// TODO yday & dst
			ret.RawSetString("yday", lua.LNumber(0))
			ret.RawSetString("isdst", lua.LFalse)
			L.Push(ret)
			return 1
		}
	}
	L.Push(lua.LString(strftime(t, cfmt)))
	return 1
}

func osGetEnv(L *lua.LState) int {
	v := os.Getenv(L.CheckString(1))
	if len(v) == 0 {
		L.Push(lua.LNil)
	} else {
		L.Push(lua.LString(v))
	}
	return 1
}

// Copy pasta: https://github.com/yuin/gopher-lua/blob/master/oslib.go#L183C1-L223C2
func osSetEnv(L *lua.LState) int {
	err := os.Setenv(L.CheckString(1), L.CheckString(2))
	if err != nil {
		L.Push(lua.LNil)
		L.Push(lua.LString(err.Error()))
		return 2
	} else {
		L.Push(lua.LTrue)
		return 1
	}
}
func osTime(L *lua.LState) int {
	if L.GetTop() == 0 {
		L.Push(lua.LNumber(time.Now().Unix()))
	} else {
		lv := L.CheckAny(1)
		if lv == lua.LNil {
			L.Push(lua.LNumber(time.Now().Unix()))
		} else {
			tbl, ok := lv.(*lua.LTable)
			if !ok {
				L.TypeError(1, lua.LTTable)
			}
			sec := getIntField(L, tbl, "sec", 0)
			min := getIntField(L, tbl, "min", 0)
			hour := getIntField(L, tbl, "hour", 12)
			day := getIntField(L, tbl, "day", -1)
			month := getIntField(L, tbl, "month", -1)
			year := getIntField(L, tbl, "year", -1)
			isdst := getBoolField(L, tbl, "isdst", false)
			t := time.Date(year, time.Month(month), day, hour, min, sec, 0, time.Local)
			// TODO dst
			if false {
				fmt.Printf("%+v", isdst)
			}
			L.Push(lua.LNumber(t.Unix()))
		}
	}
	return 1
}

// Copy Pasta from: https://github.com/yuin/gopher-lua/blob/2b3f02d9173010b12bb3ff2c0e758921accb4096/utils.go#L50-L131
type flagScanner struct {
	flag       byte
	start      string
	end        string
	buf        []byte
	str        string
	Length     int
	Pos        int
	HasFlag    bool
	ChangeFlag bool
}

func newFlagScanner(flag byte, start, end, str string) *flagScanner {
	return &flagScanner{flag, start, end, make([]byte, 0, len(str)), str, len(str), 0, false, false}
}
func (fs *flagScanner) AppendString(str string) { fs.buf = append(fs.buf, str...) }

func (fs *flagScanner) AppendChar(ch byte) { fs.buf = append(fs.buf, ch) }

func (fs *flagScanner) String() string { return string(fs.buf) }

func (fs *flagScanner) Next() (byte, bool) {
	c := byte('\000')
	fs.ChangeFlag = false
	if fs.Pos == fs.Length {
		if fs.HasFlag {
			fs.AppendString(fs.end)
		}
		return c, true
	} else {
		c = fs.str[fs.Pos]
		if c == fs.flag {
			if fs.Pos < (fs.Length-1) && fs.str[fs.Pos+1] == fs.flag {
				fs.HasFlag = false
				fs.AppendChar(fs.flag)
				fs.Pos += 2
				return fs.Next()
			} else if fs.Pos != fs.Length-1 {
				if fs.HasFlag {
					fs.AppendString(fs.end)
				}
				fs.AppendString(fs.start)
				fs.ChangeFlag = true
				fs.HasFlag = true
			}
		}
	}
	fs.Pos++
	return c, false
}

var cDateFlagToGo = map[byte]string{
	'a': "mon", 'A': "Monday", 'b': "Jan", 'B': "January", 'c': "02 Jan 06 15:04 MST", 'd': "02",
	'F': "2006-01-02", 'H': "15", 'I': "03", 'm': "01", 'M': "04", 'p': "PM", 'P': "pm", 'S': "05",
	'x': "15/04/05", 'X': "15:04:05", 'y': "06", 'Y': "2006", 'z': "-0700", 'Z': "MST"}

func strftime(t time.Time, cfmt string) string {
	sc := newFlagScanner('%', "", "", cfmt)
	for c, eos := sc.Next(); !eos; c, eos = sc.Next() {
		if !sc.ChangeFlag {
			if sc.HasFlag {
				if v, ok := cDateFlagToGo[c]; ok {
					sc.AppendString(t.Format(v))
				} else {
					switch c {
					case 'w':
						sc.AppendString(fmt.Sprint(int(t.Weekday())))
					default:
						sc.AppendChar('%')
						sc.AppendChar(c)
					}
				}
				sc.HasFlag = false
			} else {
				sc.AppendChar(c)
			}
		}
	}

	return sc.String()
}

// Copy pasta from: https://github.com/yuin/gopher-lua/blob/master/utils.go#L143C1-L156C2
func parseNumber(number string) (lua.LNumber, error) {
	var value lua.LNumber
	number = strings.Trim(number, " \t\n")
	if v, err := strconv.ParseInt(number, 0, lua.LNumberBit); err != nil {
		if v2, err2 := strconv.ParseFloat(number, lua.LNumberBit); err2 != nil {
			return lua.LNumber(0), err2
		} else {
			value = lua.LNumber(v2)
		}
	} else {
		value = lua.LNumber(v)
	}
	return value, nil
}
