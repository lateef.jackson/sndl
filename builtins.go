// This applies security to the builtin libraries
// https://github.com/yuin/gopher-lua/blob/master/baselib.go
package sndl

import (
	"context"
	"fmt"
	"strings"

	lua "github.com/yuin/gopher-lua"
)

func OpenBuiltins(L *lua.LState) int {
	global := L.Get(lua.GlobalsIndex).(*lua.LTable)
	L.SetGlobal("_G", global)
	L.SetGlobal("_VERSION", lua.LString(lua.LuaVersion))
	L.SetGlobal("_GOPHER_LUA_VERSION", lua.LString(lua.PackageName+" "+lua.PackageVersion))
	mod := L.RegisterModule("_G", builtinFuncs)
	global.RawSetString("ipairs", L.NewClosure(baseIpairs, L.NewFunction(ipairsaux)))
	global.RawSetString("pairs", L.NewClosure(basePairs, L.NewFunction(pairsaux)))
	L.Push(mod)
	return 1
}

var builtinFuncs = map[string]lua.LGFunction{
	"getmetatable": getmetatable,
	"assert":       assert,
	"print":        print,
	"require":      require,
	"select":       selectfn,
	"setmetatable": setmetatable,
	"setfenv":      setfenv,
	"type":         typefn,
	"unpack":       unpack,
}

// Copy pasta from: https://github.com/yuin/gopher-lua/blob/af7e27f2568ed807e8e55afc83cf8dcd88c3197f/baselib.go#L446
func unpack(L *lua.LState) int {
	tb := L.CheckTable(1)
	start := L.OptInt(2, 1)
	end := L.OptInt(3, tb.Len())
	for i := start; i <= end; i++ {
		L.Push(tb.RawGetInt(i))
	}
	ret := end - start + 1
	if ret < 0 {
		return 0
	}
	return ret
}

// Copy pasta from: https://github.com/yuin/gopher-lua/blob/af7e27f2568ed807e8e55afc83cf8dcd88c3197f/baselib.go#L319
func selectfn(L *lua.LState) int {
	L.CheckTypes(1, lua.LTNumber, lua.LTString)
	switch lv := L.Get(1).(type) {
	case lua.LNumber:
		idx := int(lv)
		num := L.GetTop()
		if idx < 0 {
			idx = num + idx
		} else if idx > num {
			idx = num
		}
		if 1 > idx {
			L.ArgError(1, "index out of range")
		}
		return num - idx
	case lua.LString:
		if string(lv) != "#" {
			L.ArgError(1, "invalid string '"+string(lv)+"'")
		}
		L.Push(lua.LNumber(L.GetTop() - 1))
		return 1
	}
	return 0
}

// Copy pasta from: https://github.com/yuin/gopher-lua/blob/af7e27f2568ed807e8e55afc83cf8dcd88c3197f/baselib.go#L441
func typefn(L *lua.LState) int {
	L.Push(lua.LString(L.CheckAny(1).Type().String()))
	return 1
}

// Copy pasta from: https://github.com/yuin/gopher-lua/blob/af7e27f2568ed807e8e55afc83cf8dcd88c3197f/baselib.go#L387
func setmetatable(L *lua.LState) int {

	L.CheckTypes(2, lua.LTNil, lua.LTTable)
	obj := L.Get(1)
	if obj == lua.LNil {
		L.RaiseError("cannot set metatable to a nil object.")
	}
	mt := L.Get(2)
	/*if m := L.metatable(obj, true); m != lua.LNil {
		if tb, ok := m.(*LTable); ok && tb.RawGetString("__metatable") != lua.LNil {
			L.RaiseError("cannot change a protected metatable")
		}
	}*/
	L.SetMetatable(obj, mt)
	L.SetTop(1)
	return 1
}

// COpy pasta from: https://github.com/yuin/gopher-lua/blob/af7e27f2568ed807e8e55afc83cf8dcd88c3197f/baselib.go#L344
func setfenv(L *lua.LState) int {
	var value lua.LValue
	if L.GetTop() == 0 {
		value = lua.LNumber(1)
	} else {
		value = L.Get(1)
	}
	env := L.CheckTable(2)

	if fn, ok := value.(*lua.LFunction); ok {
		if fn.IsG {
			L.RaiseError("cannot change the environment of given object")
		} else {
			fn.Env = env
			L.Push(fn)
			return 1
		}
	}

	if number, ok := value.(lua.LNumber); ok {
		level := int(float64(number))
		if level <= 0 {
			L.Env = env
			return 0

		}
		/*cf := L.currentFrame
		for i := 0; i < level && cf != nil; i++ {
			cf = cf.Parent
		}
		if cf == nil || cf.Fn.IsG {
			L.RaiseError("cannot change the environment of given object")
		} else {
			cf.Fn.Env = env
			L.Push(cf.Fn)
			return 1
		}
		*/
	}

	L.RaiseError("cannot change the environment of given object")
	return 0
}

// Copy pasta from: https://github.com/yuin/gopher-lua/blob/master/baselib.go#L58
func assert(L *lua.LState) int {
	if !L.ToBool(1) {
		L.RaiseError(L.OptString(2, "assertion failed!"))
		return 0
	}
	return L.GetTop()
}

// Copy pasta from: https://github.com/yuin/gopher-lua/blob/master/baselib.go#L238
func pairsaux(L *lua.LState) int {
	tb := L.CheckTable(1)
	key, value := tb.Next(L.Get(2))
	if key == lua.LNil {
		return 0
	} else {
		L.Pop(1)
		L.Push(key)
		L.Push(key)
		L.Push(value)
		return 2
	}
}

func basePairs(L *lua.LState) int {
	tb := L.CheckTable(1)
	L.Push(L.Get(lua.UpvalueIndex(1)))
	L.Push(tb)
	L.Push(lua.LNil)
	return 3
}

// Copy pasta from: https://github.com/yuin/gopher-lua/blob/master/baselib.go#L135
func ipairsaux(L *lua.LState) int {
	tb := L.CheckTable(1)
	i := L.CheckInt(2)
	i++
	v := tb.RawGetInt(i)
	if v == lua.LNil {
		return 0
	} else {
		L.Pop(1)
		L.Push(lua.LNumber(i))
		L.Push(lua.LNumber(i))
		L.Push(v)
		return 2
	}
}

func baseIpairs(L *lua.LState) int {
	tb := L.CheckTable(1)
	L.Push(L.Get(lua.UpvalueIndex(1)))
	L.Push(tb)
	L.Push(lua.LNumber(0))
	return 3
}

// Copy pasta from: https://github.com/yuin/gopher-lua/blob/master/baselib.go#L130
func getmetatable(L *lua.LState) int {
	L.Push(L.GetMetatable(L.CheckAny(1)))
	return 1
}

// Copy pasta from: https://github.com/yuin/gopher-lua/blob/master/baselib.go#L283
func print(L *lua.LState) int {
	top := L.GetTop()
	for i := 1; i <= top; i++ {
		fmt.Print(L.ToStringMeta(L.Get(i)).String())
		if i != top {
			fmt.Print("\t")
		}
	}
	fmt.Println("")
	return 0
}

var loopdetection = &lua.LUserData{}

func require(L *lua.LState) int {
	name := L.CheckString(1)
	cfg := getConfig(L)
	_, hasConfig := cfg.Sockets[name]
	if hasConfig {
		ctx := L.Context()
		nc := context.WithValue(ctx, CallerModuleKey, name)
		L.SetContext(nc)
	}
	loaded := L.GetField(L.Get(lua.RegistryIndex), "_LOADED")
	lv := L.GetField(loaded, name)
	if lua.LVAsBool(lv) {
		if lv == loopdetection {
			L.RaiseError("loop or previous error loading module: %s", name)
		}
		L.Push(lv)
		return 1
	}
	loaders, ok := L.GetField(L.Get(lua.RegistryIndex), "_LOADERS").(*lua.LTable)
	if !ok {
		L.RaiseError("package.loaders must be a table")
	}
	messages := []string{}
	var modasfunc lua.LValue
	for i := 1; ; i++ {
		loader := L.RawGetInt(loaders, i)
		if loader == lua.LNil {
			L.RaiseError("module %s not found:\n\t%s, ", name, strings.Join(messages, "\n\t"))
		}
		L.Push(loader)
		L.Push(lua.LString(name))
		L.Call(1, 1)
		ret := L.Get(L.GetTop())
		L.Pop(L.GetTop())
		switch retv := ret.(type) {
		case *lua.LFunction:
			modasfunc = retv
			goto loopbreak
		case lua.LString:
			messages = append(messages, string(retv))
		}
	}
loopbreak:
	L.SetField(loaded, name, loopdetection)
	L.Push(modasfunc)
	L.Push(lua.LString(name))
	L.Call(1, 1)
	ret := L.Get(L.GetTop())
	L.Pop(L.GetTop())
	modv := L.GetField(loaded, name)
	if ret != lua.LNil && modv == loopdetection {
		L.SetField(loaded, name, ret)
		L.Push(ret)
	} else if modv == loopdetection {
		L.SetField(loaded, name, lua.LTrue)
		L.Push(lua.LTrue)
	} else {
		L.Push(modv)
	}
	return 1
}
