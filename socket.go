package sndl

// The intent is to implement a socket API like the luasocket package (https://w3.impa.br/~diego/software/luasocket/tcp.html)

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net"
	"strconv"
	"strings"
	"time"

	lua "github.com/yuin/gopher-lua"
)

type Host struct {
	Domain string `json:"domain"`
	Ports  []int  `json:"ports"`
	Read   bool   `json:"read"`
	Write  bool   `json:"write"`
}

type SocketAcl struct {
	Module string `json:"package"`
	Allow  []Host `json:"allow"`
}

func findSocketAcl(L *lua.LState) *SocketAcl {
	cfg := getConfig(L)
	if cfg != nil {
		s, exists := cfg.Sockets[getCallingPackage(L)]
		if exists {
			return &s
		}
	}
	return nil
}

func (h *Host) portAllowed(p int) bool {
	for _, pt := range h.Ports {
		if p == pt {
			return true
		}
	}
	return false
}

func ParseSocketAclConfig(txt string) (*SocketAcl, error) {

	var sa SocketAcl
	err := json.Unmarshal([]byte(txt), &sa)
	if err != nil {
		return nil, err
	}

	return &sa, nil
}

func (sa *SocketAcl) Name() string { return socketModule }

func (sa *SocketAcl) findHost(n string, p int) *Host {
	for _, h := range sa.Allow {
		if strings.HasSuffix(n, h.Domain) && h.portAllowed(p) {
			return &h
		}
	}
	return nil
}

func (sa *SocketAcl) Readable(n string, p int) bool {
	h := sa.findHost(n, p)
	if h != nil {
		return h.Read
	}
	return false
}

func (sa *SocketAcl) Writeable(n string, p int) bool {
	h := sa.findHost(n, p)
	if h != nil {
		return h.Write
	}
	return false
}

const luaTcpConnTypeName = "tcpConn"

// Socket library
const luaSocketTypeName = "socket"

type tcpConn struct {
	host    string
	conn    net.Conn
	port    int
	timeout time.Duration
	reader  *bufio.Reader
	writer  *bufio.Writer
}

func (tcpc *tcpConn) connect(host string, port int) error {
	c, err := net.Dial("tcp", fmt.Sprintf("%s:%d", host, port))
	if err != nil {
		return err
	}
	tcpc.conn = c
	tcpc.host = host
	tcpc.port = port
	return nil
}

func checkTcpConn(L *lua.LState) *tcpConn {
	ud := L.CheckUserData(1)
	if v, ok := ud.Value.(*tcpConn); ok {
		return v
	}
	L.ArgError(1, "tcp expected ")
	return nil
}

func connect(L *lua.LState) int {
	tcpc := checkTcpConn(L)
	host := L.CheckString(2)
	port := L.CheckInt(3)

	s := findSocketAcl(L)
	if s == nil {
		m := getCallingPackage(L)
		msg := fmt.Sprintf("Host %s is not in configuration for module: '%s'", host, m)
		log.Println(msg)
		L.Push(lua.LNil)
		L.Push(lua.LString(msg))
		return 2

	} else {
		if !s.Readable(host, port) {
			L.Push(lua.LNil)
			L.Push(lua.LString(fmt.Sprintf("Host %s is not readable based on configuration", host)))
			return 2
		}
	}
	// Need to actuall connect to the socket
	err := tcpc.connect(host, port)
	if err != nil {
		log.Printf("Error connecting to host %s port %d: %s\n", tcpc.host, tcpc.port, err)
		L.Push(lua.LNil)
		L.Push(lua.LString(err.Error()))
		return 2
	}
	L.Push(lua.LNumber(1))
	L.Push(lua.LNil)
	return 2
}

func settimeout(L *lua.LState) int {
	tcpc := checkTcpConn(L)
	timeout := L.CheckInt64(2)
	// XXX: This assumes seconds which need to be confirmed with lua spec
	tcpc.timeout = time.Duration(timeout) * time.Second
	return 1
}

func tcpToString(L *lua.LState) int {
	tcpc := checkTcpConn(L)
	L.Push(lua.LString(fmt.Sprintf("%+v", tcpc)))
	return 1
}
func send(L *lua.LState) int {
	tcpc := checkTcpConn(L)
	data := L.CheckString(2)
	// TODO: The optional arguments i and j work exactly like the standard string.sub Lua function to allow the selection of a substring to be sent.
	if tcpc.timeout.Abs() > 0 {
		err := tcpc.conn.SetDeadline(time.Now().Add(tcpc.timeout))
		if err != nil {
			log.Printf("Error setting timeout to host %s port %d: %s\n", tcpc.host, tcpc.port, err)
			L.Push(lua.LNumber(0))
			L.Push(lua.LString(err.Error()))
			return 2
		}
	}
	if tcpc.writer == nil {
		tcpc.writer = bufio.NewWriter(tcpc.conn)
	}
	written, err := tcpc.writer.Write([]byte(data))
	if err != nil {
		log.Printf("Error sending to host %s port %d: %s\n", tcpc.host, tcpc.port, err)
		L.Push(lua.LNumber(0))
		L.Push(lua.LString(err.Error()))
		return 2
	}
	tcpc.writer.Flush()
	L.Push(lua.LNumber(written))
	L.Push(lua.LNil)
	return 2
}

func receive(L *lua.LState) int {
	tcpc := checkTcpConn(L)
	if tcpc.timeout.Abs() > 0 {
		err := tcpc.conn.SetDeadline(time.Now().Add(tcpc.timeout))
		if err != nil {
			L.ArgError(1, err.Error())
			log.Printf("Error setting timeout to host %s port %d: %s\n", tcpc.host, tcpc.port, err)
			return 0
		}
	}
	if tcpc.reader == nil {
		tcpc.reader = bufio.NewReader(tcpc.conn)
	}
	var status lua.LValue
	status = lua.LNil
	d, partial, err := tcpc.reader.ReadLine()
	var data lua.LValue
	if err != nil {
		status = lua.LString("closed")
		data = lua.LNil
		if err != io.EOF {
			L.Push(lua.LNil)
			L.Push(status)
			L.Push(lua.LBool(partial))
			L.ArgError(1, err.Error())
			log.Printf("Error receiving to host %s port %d: %s\n", tcpc.host, tcpc.port, err)
			return 3
		}
	}

	if strings.TrimSpace(string(d)) != "" {
		data = lua.LString(string(d))
	}
	if partial {
		L.Push(lua.LNil)
		L.Push(status)
		L.Push(data)
	} else {
		L.Push(data)
		L.Push(status)
		L.Push(lua.LNil)
	}
	return 3
}

func close(L *lua.LState) int {
	tcpc := checkTcpConn(L)
	err := tcpc.conn.Close()
	if err != nil {
		L.ArgError(1, err.Error())
		log.Printf("Error closing connection to host %s port %d: %s\n", tcpc.host, tcpc.port, err)
		return 0
	}
	return 1
}

var tcpConnMethods = map[string]lua.LGFunction{
	"connect":    connect,
	"send":       send,
	"receive":    receive,
	"settimeout": settimeout,
	"close":      close,
	"__tostring": tcpToString,
	// TODO "getsockname": getsockname,
	// TODO: "setoption": setoption, // keepalive, linger, resuaddr, tcp-nodelay
	// TODO "shutdown": shutdown, // both, send, receive
}

func tcp(L *lua.LState) int {
	tcpc := &tcpConn{}
	ud := L.NewUserData()
	ud.Value = tcpc
	L.SetMetatable(ud, L.GetTypeMetatable(luaTcpConnTypeName))
	L.Push(ud)
	return 1
}

type serverSocket struct {
	host string
	sock net.Listener
	port string
}

func checkServerSocket(L *lua.LState) *serverSocket {
	ud := L.CheckUserData(1)
	if v, ok := ud.Value.(*serverSocket); ok {
		return v
	}
	L.ArgError(1, "serverSocket expected ")
	return nil
}

func getsockname(L *lua.LState) int {
	ss := checkServerSocket(L)
	h, p, err := net.SplitHostPort(ss.sock.Addr().String())
	if err != nil {
		L.ArgError(1, err.Error())
		log.Printf("Error SplitHostPort host %s port %s : %s\n", ss.host, ss.port, err)
		return 0
	}
	if h == "::" {
		h = "*"
	}
	L.Push(lua.LString(h))
	L.Push(lua.LString(p))
	return 2
}

func accept(L *lua.LState) int {
	ss := checkServerSocket(L)
	conn, err := ss.sock.Accept()
	if err != nil {
		L.ArgError(1, err.Error())
		log.Printf("Error accepting server connect host %s port %s: %s\n", ss.host, ss.port, err)
		return 0
	}

	// TODO: Do something with error
	port, _ := strconv.Atoi(ss.port)
	tcpc := &tcpConn{conn: conn, host: ss.host, port: port}
	ud := L.NewUserData()
	ud.Value = tcpc
	L.SetMetatable(ud, L.GetTypeMetatable(luaTcpConnTypeName))
	L.Push(ud)
	return 1
}

var serverMethods = map[string]lua.LGFunction{
	"getsockname": getsockname,
	"accept":      accept,
	// TODO "setstats": setstats,
	// TODO "getstats": getstats,
	// TODO "settimeout": settimeout,
}

const luaServerSocketTypeName = "serverSocket"

func bind(L *lua.LState) int {
	host := L.CheckString(1)
	port := L.CheckInt(2)
	addr := fmt.Sprintf("%s:%d", host, port)
	// Support wild cardd
	if host == "*" {
		addr = ":"
	}
	s, err := net.Listen("tcp", addr)
	if err != nil {
		L.ArgError(1, err.Error())
		log.Printf("Error binding to host %s port %d: %s\n", host, port, err)
		return 0
	}
	h, p, err := net.SplitHostPort(s.Addr().String())
	if err != nil {
		L.ArgError(1, err.Error())
		log.Printf("Error binding to host %s port %s: %s\n", h, p, err)
		return 0
	}

	ss := &serverSocket{host: h, port: p, sock: s}
	ud := L.NewUserData()
	ud.Value = ss
	L.SetMetatable(ud, L.GetTypeMetatable(luaServerSocketTypeName))
	L.Push(ud)
	return 1

}

var socketFuncs = map[string]lua.LGFunction{
	"bind": bind,
	"tcp":  tcp,
}

const socketModule = "socket"

func OpenSecureSocket(L *lua.LState) int {
	mod := L.RegisterModule(socketModule, socketFuncs).(*lua.LTable)

	mt := L.NewTypeMetatable(luaTcpConnTypeName)
	L.SetGlobal(luaTcpConnTypeName, mt)
	mt.RawSetString("__index", mt)
	L.SetFuncs(mt, tcpConnMethods)

	serverMt := L.NewTypeMetatable(luaServerSocketTypeName)
	L.SetGlobal(luaServerSocketTypeName, serverMt)
	serverMt.RawSetString("__index", serverMt)
	L.SetFuncs(serverMt, serverMethods)

	L.Push(mod)
	return 1
}
