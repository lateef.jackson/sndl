package chat

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"time"

	_ "github.com/mattn/go-sqlite3"
	lua "github.com/yuin/gopher-lua"
	"gitlab.com/lateef.jackson/sndl/dsl"
)

const (
	ChatLibName        = "chat"
	googleChatTypeName = "chatGoogle"
)

var chatFuncs = map[string]lua.LGFunction{
	"google": newGoogleChat,
}
var googleFuncs = map[string]lua.LGFunction{
	"send": googleSend,
}

type googleChat struct {
	Webhook string
}

func checkGoogle(L *lua.LState) *googleChat {
	ud := L.CheckUserData(1)
	if v, ok := ud.Value.(*googleChat); ok {
		return v
	}
	L.ArgError(1, "client not found")
	return nil
}
func newGoogleChat(L *lua.LState) int {
	wh := L.CheckString(1)
	gc := &googleChat{Webhook: wh}
	ud := L.NewUserData()
	ud.Value = gc
	L.SetMetatable(ud, L.GetTypeMetatable(googleChatTypeName))
	L.Push(ud)
	return 1
}

func googleSend(L *lua.LState) int {
	gc := checkGoogle(L)
	vi := dsl.ValueToGoInterface(L, L.CheckTable(2)).(map[string]interface{})
	bits, err := json.Marshal(vi)
	if err != nil {
		L.Push(lua.LNil)
		L.Push(lua.LString(fmt.Sprintf("Error: %s", err)))
		return 2
	}
	u := gc.Webhook
	thr, ok := vi["thread"]
	if ok {
		tk := thr.(map[string]interface{})["threadKey"]
		u = u + "&threadKey=" + url.QueryEscape(tk.(string)) + "&messageReplyOption=REPLY_MESSAGE_FALLBACK_TO_NEW_THREAD"
	}
	req, err := http.NewRequest("POST", u, bytes.NewReader(bits))
	if err != nil {
		L.Push(lua.LNil)
		L.Push(lua.LString(fmt.Sprintf("Error: %s", err)))
		return 2
	}
	req.Header.Set("Content-Type", "application/json")
	client := http.Client{Timeout: 10 * time.Second}
	// Send request
	res, err := client.Do(req)
	if err != nil {
		L.Push(lua.LNil)
		L.Push(lua.LString(fmt.Sprintf("Error: %s", err)))
		return 2
	}
	defer res.Body.Close()
	var jd map[string]interface{}
	json.NewDecoder(res.Body).Decode(&jd)
	t := dsl.MapToTable(L, jd)
	L.Push(t)
	L.Push(lua.LNil)
	return 2
}
func OpenChat(L *lua.LState) int {
	mod := L.RegisterModule(ChatLibName, chatFuncs).(*lua.LTable)

	gt := L.NewTypeMetatable(googleChatTypeName)
	L.SetGlobal(googleChatTypeName, gt)
	gt.RawSetString("__index", gt)
	L.SetFuncs(gt, googleFuncs)

	L.Push(mod)
	return 1
}
