package sndl

import (
	"bytes"
	"fmt"
	"path/filepath"

	mustache "github.com/cbroglie/mustache"
	lua "github.com/yuin/gopher-lua"
	"gitlab.com/lateef.jackson/sndl/dsl"
)

const (
	StacheLibName = "stache"
)

var fileProvider *mustache.FileProvider

func init() {
	fileProvider = &mustache.FileProvider{Paths: []string{""}, Extensions: []string{"", ".html"}}

}
func appendIfMissingPath(fp *mustache.FileProvider, path string) {
	for _, p := range fp.Paths {
		if path == p {
			return
		}
	}
	fp.Paths = append(fp.Paths, path)
}

// Caching template parsing benchmarked as a 20% increase in requests per second. Watching for any file changes and clearing the cache could provide a balanced between performance and developer experience.
func renderFile(path string, context interface{}) ([]byte, error) {
	d := filepath.Dir(path)
	appendIfMissingPath(fileProvider, d)
	var buf bytes.Buffer
	tmpl, err := mustache.ParseFilePartials(path, fileProvider)
	if err != nil {
		return nil, err
	}
	tmpl.FRender(&buf, context)
	return buf.Bytes(), nil
}

func stacheRenderFile(L *lua.LState) int {
	path := L.CheckString(1)
	context := L.CheckTable(2)
	m := dsl.ValueToGoInterface(L, context)
	bits, err := renderFile(path, m)
	if err != nil {
		L.Push(lua.LNil)
		L.Push(lua.LString(fmt.Sprintf("Error parsing template %s", err)))
		return 2
	}

	L.Push(lua.LString(string(bits)))
	L.Push(lua.LNil)
	return 2
}
func stacheTemplatePath(L *lua.LState) int {
	path := L.CheckString(1)
	fileProvider.Paths = append(fileProvider.Paths, path)
	return 0
}

func stacheTemplateExtension(L *lua.LState) int {
	ext := L.CheckString(1)
	fileProvider.Extensions = append(fileProvider.Extensions, ext)
	return 0
}

var stacheFuncs = map[string]lua.LGFunction{
	"render_file":        stacheRenderFile,
	"template_path":      stacheTemplatePath,
	"template_extension": stacheTemplateExtension,
}

func OpenStache(L *lua.LState) int {
	mod := L.RegisterModule(StacheLibName, stacheFuncs).(*lua.LTable)
	L.Push(mod)
	return 1
}
