package sndl

import (
	"context"
	"testing"

	lua "github.com/yuin/gopher-lua"
)

func TestSocketAcl(t *testing.T) {
	config := `
{
	"package": "testexample",
  "allow": [
    {
      "domain": "lhj.me",
      "ports": [2100],
      "read": true,
      "write": true
    }
  ]
}
`
	sa, err := ParseSocketAclConfig(config)
	if err != nil {
		t.Fatalf("Error parsing config: %s", err)
	}
	if sa.Module != "testexample" {
		t.Fatalf("Expected module to be 'testexample' however it is %s", sa.Module)
	}
	h := Host{Domain: "lhj.me", Ports: []int{2100}, Read: true, Write: true}
	//sa := SocketAcl{Allow: []Host{h}}
	allowDomain := "www.lhj.me"
	denyDomain := "www.lateefjackson.com"
	allowPort := 2100
	fh1 := sa.findHost(allowDomain, allowPort)
	if fh1 == nil {
		t.Fatalf("Expected to find matching domain: %s, and port %d", h.Domain, allowPort)
	}
	pfh := sa.findHost(allowDomain, allowPort+1)
	if pfh != nil {
		t.Fatalf("Port %d should not be in ports %+v ", allowPort+1, h.Ports)
	}
	fh2 := sa.findHost(denyDomain, allowPort)
	if fh2 != nil {
		t.Fatalf("Did not expect to match domain: %s", h.Domain)
	}

	if !sa.Readable(allowDomain, allowPort) {
		t.Fatalf("Expected readbale domain %s with allowed domain %s", allowDomain, h.Domain)
	}

	if sa.Readable(denyDomain, allowPort) {
		t.Fatalf("Should not be able to read %s with when allowed domain %s", denyDomain, h.Domain)
	}
	if !sa.Writeable(allowDomain, allowPort) {
		t.Fatalf("Expected writeable domain %s with allowed domain %s", allowDomain, h.Domain)
	}

	if sa.Writeable(denyDomain, allowPort) {
		t.Fatalf("Should not be able to write %s with when allowed domain %s", denyDomain, h.Domain)
	}
}

func TestConfigLookup(t *testing.T) {
	L := lua.NewState(lua.Options{SkipOpenLibs: true})
	th := Host{
		Domain: "lhj.me",
		Ports:  []int{2100},
		Read:   true,
		Write:  true,
	}
	c := context.WithValue(context.Background(), configKey, &Config{
		Sockets: map[string]SocketAcl{
			"testing": {
				Allow: []Host{th},
			},
		},
	},
	)
	c = context.WithValue(c, CallerModuleKey, "testing")
	L.SetContext(c)
	sa := findSocketAcl(L)
	if sa == nil {
		t.Fatal("Expected to find socket for modul 'testing'")
	}

	h := sa.findHost(th.Domain, th.Ports[0])
	if h == nil {
		t.Fatal("Failed to find host for know good domain and port")

	}
	if !sa.Readable(th.Domain, th.Ports[0]) || !sa.Writeable(th.Domain, th.Ports[0]) {
		t.Fatalf("Expected host to be readbale and writable however it is %+v", h)
	}
}

func TestConnect(t *testing.T) {
	L := lua.NewState(lua.Options{SkipOpenLibs: true})
	th := Host{
		Domain: "google.com",
		Ports:  []int{80},
		Read:   true,
		Write:  true,
	}
	c := context.WithValue(context.Background(), configKey, &Config{
		Sockets: map[string]SocketAcl{
			"testing": {
				Allow: []Host{th},
			},
		},
	},
	)
	unsecc := context.WithValue(c, CallerModuleKey, "unsecure")
	L.SetContext(unsecc)
	tcp(L)

	conn := L.Get(L.GetTop())
	if conn == lua.LNil {
		t.Fatal("Nil connection returned")
	}
	L.Pop(L.GetTop())

	L.Push(conn)
	L.Push(lua.LString(th.Domain))
	L.Push(lua.LNumber(th.Ports[0]))
	connect(L)
	lerr := L.Get(L.GetTop())
	if lerr == lua.LNil {
		t.Fatal("Expected error trying to connect to 'unsecure' calling package ")
	}
	L.Pop(L.GetTop())
	secc := context.WithValue(c, CallerModuleKey, "testing")
	L.SetContext(secc)
	L.Push(conn)
	L.Push(lua.LString(th.Domain))
	L.Push(lua.LNumber(th.Ports[0]))
	connect(L)
	lerr2 := L.Get(L.GetTop())
	if lerr2 != lua.LNil {
		t.Fatalf("Expected no error from tcp however recieved %+v", lerr)
	}

	L.Pop(L.GetTop())
}
