package dsl

import (
	"errors"
	"fmt"
	"time"

	lua "github.com/yuin/gopher-lua"
)

func ToGoTime(l lua.LNumber) time.Time {
	return time.Unix(int64(l), 0)
}

func ToLuaTime(t time.Time) lua.LNumber {
	return lua.LNumber(t.Unix())
}
func ValueToGoInterface(L *lua.LState, value lua.LValue) interface{} {
	switch t := value.Type(); t {
	case lua.LTNil:
		return nil
	case lua.LTBool:
		return lua.LVAsBool(value)
	case lua.LTNumber:
		f := lua.LVAsNumber(value)
		if float64(f) == float64(int(f)) {
			return int(f)
		}
		return float64(f)
	case lua.LTString:
		return lua.LVAsString(value)
	case lua.LTTable:
		tbl := value.(*lua.LTable)
		// It is a map
		m := map[string]interface{}{}
		a := []interface{}{}
		tbl.ForEach(func(k, v lua.LValue) {
			idx := ValueToGoInterface(L, k)
			switch idx.(type) {
			case string:
				key := idx.(string)
				m[key] = ValueToGoInterface(L, v)
			case int:
				a = append(a, ValueToGoInterface(L, v))
			}
		})
		if len(a) > 0 {
			return a
		}
		return m
	}
	return errors.New(fmt.Sprintf("Type '%s' not supported", value.Type()))
}

func MapToTable(L *lua.LState, m map[string]interface{}) *lua.LTable {
	tbl := L.NewTable()
	for k, v := range m {
		switch v.(type) {
		case nil:
			tbl.RawSetH(lua.LString(k), lua.LNil)
		case int:
			tbl.RawSetH(lua.LString(k), lua.LNumber(v.(int)))
		case int32:
			tbl.RawSetH(lua.LString(k), lua.LNumber(v.(int32)))
		case int64:
			tbl.RawSetH(lua.LString(k), lua.LNumber(v.(int64)))
		case float32:
			tbl.RawSetH(lua.LString(k), lua.LNumber(v.(float32)))
		case float64:
			tbl.RawSetH(lua.LString(k), lua.LNumber(v.(float64)))
		case string:
			tbl.RawSetString(k, lua.LString(v.(string)))
		case map[string]interface{}:
			tbl.RawSetH(lua.LString(k), MapToTable(L, v.(map[string]interface{})))
		}
	}
	return tbl
}
