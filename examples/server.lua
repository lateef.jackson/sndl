-- load namespace
local socket = require("socket")

-- create a TCP socket and bind it to the local host, at any port
local server = assert(socket.bind("localhost", 2100))
-- find out which port the OS chose for us
local ip, port = server:getsockname()
-- print a message informing what's up
print("Host ip is " .. ip)
--print("Please telnet to localhost on port " .. port)
print("After connecting, you have 10s to enter a line to be echoed")
-- loop forever waiting for clients
while 1 do
	-- wait for a connection from any client
	local client = server:accept()
	for k, v in pairs(getmetatable(client).__index) do
		print(k, v)
	end
	-- make sure we don't block waiting for this client's line
	client:settimeout(10)

	-- receive the line
	local line, err = client:receive()
	-- if there was no error, send it back to the client
	if not err then
		print("sending line to client " .. line .. "\n")
		client:send(line .. "\n")
	end
	-- done with client, close the object
	client:close()
end
