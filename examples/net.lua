local socket = require("socket")

local net = {}

function net:connect(host, port)
  local tcp = socket.tcp()
  tcp:connect(host, port)
  return tcp
end

return net
