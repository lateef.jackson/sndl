--[[
Simple curl command to publish a change

curl -X POST 'http://localhost:7070/change' -H 'Content-Type: application/json' -d '{"name":"foo", "type": "deploy", "repo": "repo","start_time": "2023-08-02T17:25:58Z", "end_time":"2023-08-02T17:36:07Z", "settle_delay": "15m",  "labels": {"foo":"bar", "service":"website"}}'

This message will not match

curl -X POST "http://localhost:7070/change" -H 'Content-Type: application/json' -d '{"name":"foo", "repo": "repo", "type": "deploy", "labels": {"foo":"bar", "service":"website"}}'

--]]
--
local delta = require("delta")
local math = require("math")

local prom = require("prom")

local minute = 60
local s = delta.stream()

local cpu_metric_name = "node_cpu_seconds_total"

s:handle({
  name = "foo",
  handler = function(cng)
    print(cng.type .. "->")
    print("  Name is " .. cng.name)
    print("  Repo is " .. cng.repo)
    print("  Start Time " .. cng.start_time)
    print("  Labels: ")
    for k, v in pairs(cng.labels) do
      print("    label key is '" .. k .. "' value is '" .. v .. "'")
    end

    local client, err = prom:new()
    if err ~= nil then
      print("Error creating prom client: " .. err)
    end

    local pre_deploy = 0
    local sig, merr = client:signal(cpu_metric_name, {
      labels = { cluster = "homelab" },
      range = {
        start_time = (cng.start_time - (30 * minute)),
        end_time = cng.start_time - (20 * minute),
        step = "15s",
      },
    })
    if merr ~= nil then
      print("Yikes error getting signal: " .. merr)
      return
    else
      pre_deploy = sig:delta():max()
      print("Max node_cpu_seconds_total is: " .. string.format("%.4f", sig:delta():max()))
    end
    local post_sig, perr = client:signal(cpu_metric_name, {
      labels = { cluster = "homelab" },
      range = { start_time = (cng.start_time - (10 * minute)), end_time = cng.end_time, step = "30s" },
    })
    if perr ~= nil then
      print("Yikes error getting signal: " .. perr)
      return
    else
      local post_deploy = post_sig:delta():max()
      print("Max post deploy node_cpu_seconds_total is: " .. string.format("%.4f", post_sig:delta():max()))
      local d = math.abs(post_deploy - pre_deploy)
      if d > 0.25 then
        print(string.format("Yikes post deploy CPU is UP!!! @ %0.4f ", d))
      else
        print(string.format("Post deploy cpu is not to high @ %0.4f ", d))
      end
    end
  end,
})

s:listen(":7070", "/change")
