local os = require("os")

Book = {}
Book.__index = Book

function Book:create(size)
	local l = {}
	setmetatable(l, Book)
	l.size = size
	l.guests = {}
	return l
end

function Book:add(name)
	if name == nil then
		return
	end
	for _, g in pairs(self.guests) do
		if g.name == name then
			return
		end
	end
	if #self.guests > self.size then
		table.remove(self.guests, 1)
	end
	table.insert(
		self.guests,
		#self.guests + 1,
		{ name = name, added = os.time(), created = os.date("%Y-%m-%d %H:%M:%S") }
	)
end

function Book:recent_guests(limit)
	local rec = {}
	local x = #self.guests
	while x > 0 do
		local g = self.guests[x]
		table.insert(rec, g)
		x = x - 1
		if x < #self.guests - limit then
			break
		end
	end
	return rec
end

return Book
