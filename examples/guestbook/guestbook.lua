local http = require("http")
local stache = require("stache")
local book = require("book")

local gb = book:create(96)

local function process_form(form)
	if form == nil or form["name"] == nil then
		return
	end
	for _, n in pairs(form["name"]) do
		gb:add(n)
	end
end

http.match("/", function(resp, req)
	process_form(req.form)
	resp:status(200)
	local html, rerr = stache.render_file("examples/guestbook/index.html", { guests = gb:recent_guests(16) })
	if rerr ~= nil then
		resp:write("Error rendering page: " .. rerr)
	else
		resp:write(html)
	end
end)

http.publish_metrics("/metrics")
local err = http.listen(":7002")
if err ~= nil then
	print("Failed to listen error: " .. err)
end
