local host, port = "127.0.0.1", 2100
local net = require("net")

local tcp = net:connect(host, port)
--note the newline below
tcp:send("hello world\n")

while true do
  local s, status, partial = tcp:receive()
  if s or partial then
    print("LUA: Client recieved data: ")
    print(s or partial)
  end
  if status ~= nil then
    print("LUA: Status is: " .. status)
  end
  if status == "closed" then
    break
  end
end
tcp:close()
