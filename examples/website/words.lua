local math = require("math")
local string = require("string")
local io = require("io")

local words = {}

local dictionary_path = "/usr/share/dict/words"
local f = io.open(dictionary_path, "r")
if f == nil then
	dictionary_path = "/usr/dict/words"
end

local wdlst = {}
for line in io.lines(dictionary_path) do
	if line ~= nil then
		local wl = string.len(line)
		if wdlst[wl] == nil then
			wdlst[wl] = {}
		end
		local lst = wdlst[wl]
		lst[#lst + 1] = string.upper(line)
	end
end

words.random_word = function(word_size)
	return wdlst[word_size][math.random(#wdlst[word_size])]
end

words.guess = function(correct_word, letters)
	local found = {}
	for i = 1, string.len(letters) do
		local c = string.upper(string.sub(letters, i, i))
		local status = "WRONG"
		if string.find(correct_word, c) then
			if c == string.sub(correct_word, i, i) then
				status = "CORRECT"
			else
				status = "EXISTS"
			end
		end
		found[c] = status
	end
	return found
end

print('Guess "FRUIT" correct letter for word GRIFT: ')
for c, status in pairs(words.guess("GRIFT", "FRUIT")) do
	print("Status of " .. c .. " is: " .. status)
end

print("Number of 6 letter words " .. #wdlst[6])
for i = 1, 10 do
	print("Random word:" .. words.random_word(6))
end

return words
