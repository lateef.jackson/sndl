local http = require("http")
local stache = require("stache")
local math = require("math")

http.match("/", function(resp, req)
	resp:status(200)
	local html, rerr = stache.render_file("examples/website/index.html", { random_num = math.random(1, 10000000) })
	if rerr ~= nil then
		resp:write("Error rendering page " .. rerr)
	else
		resp:write(html)
	end
end)

http.publish_metrics("/metrics")
http.listen(":7001")
