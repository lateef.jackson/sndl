local sqlite3 = require("sqlite3")

local db = sqlite3.new()
local connected = db:connect(":memory:")
if not connected then
	print("Error: failed to connect to db")
end

db:query("CREATE TABLE temp (x INT, y INT);")
db:query("INSERT INTO temp (x, y) VALUES(1, 2);")
local resp, resperr = db:query("SELECT * FROM temp;")
if resperr ~= nil then
	print("Error query: " .. resperr)
else
	for c, r in pairs(resp) do
		for k, v in pairs(r) do
			print(string.format("k %s v %s", k, v))
		end
	end
end
db:connect("/Users/lhj/Downloads/census2000names.sqlite")
local resp, resperr = db:query("SELECT * FROM surnames LIMIT 10;")
if resperr ~= nil then
	print("Error query: " .. resperr)
else
	for _, r in pairs(resp) do
		for k, v in pairs(r) do
			print(string.format("k %s v %s", k, v))
		end
	end
end
