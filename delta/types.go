package delta

import (
	"encoding/json"
	"fmt"
	"regexp"
	"time"
)

type ChangeType string

const (
	DeployChange ChangeType = "Deploy"
	ConfigChange ChangeType = "Configuration"
)

type Change struct {
	Key         string                 `json:"key"`
	Type        ChangeType             `json:"type"`
	Name        string                 `json:"name"`
	Description string                 `json:"description"`
	StartTime   time.Time              `json:"start_time"`
	EndTime     time.Time              `json:"end_time"`
	SettleDelay time.Duration          `json:"settle_delay"`
	Repo        string                 `json:"repo"`
	ChangeSet   string                 `json:"change_set"`
	Labels      map[string]string      `json:"labels"`
	Data        map[string]interface{} `json:"data"`
}

func UnmarshalChange(bits []byte) (Change, error) {

	// XXX: Work around time.Duration does not have support for Unmarshal
	// Simple wrapper for Change struct that has a string instead of time.Duration for field SettleDelay
	tmp := struct {
		Key         string                 `json:"key"`
		Type        ChangeType             `json:"type"`
		Name        string                 `json:"name"`
		Description string                 `json:"description"`
		StartTime   time.Time              `json:"start_time"`
		EndTime     time.Time              `json:"end_time"`
		SettleDelay string                 `json:"settle_delay"`
		Repo        string                 `json:"repo"`
		ChangeSet   string                 `json:"change_set"`
		Labels      map[string]string      `json:"labels"`
		Data        map[string]interface{} `json:"data"`
	}{}
	err := json.Unmarshal(bits, &tmp)
	if err != nil {
		return Change{}, err
	}
	sd, err := time.ParseDuration(tmp.SettleDelay)
	if err != nil {
		return Change{}, err
	}

	c := Change{
		Key:         tmp.Key,
		Type:        tmp.Type,
		Name:        tmp.Name,
		Description: tmp.Description,
		StartTime:   tmp.StartTime,
		EndTime:     tmp.EndTime,
		SettleDelay: sd,
		Repo:        tmp.Repo,
		ChangeSet:   tmp.ChangeSet,
		Labels:      tmp.Labels,
		Data:        tmp.Data,
	}
	return c, nil
}

type ChangeMatch struct {
	Name   *regexp.Regexp
	Type   string
	Labels map[string]string
	Repo   string
}

func (m *ChangeMatch) Match(c *Change) bool {

	fmt.Printf("Match is %+v changs is %+v\n", m, c)
	// Default matches all
	if m.Name == nil && m.Type == "" && len(m.Labels) == 0 && m.Repo == "" {
		return true
	}
	if c.Name != "" && m.Name != nil && !m.Name.Match([]byte(c.Name)) {
		return false
	}
	if m.Type != "" && m.Type != string(c.Type) {
		return false
	}
	if m.Repo != "" && m.Repo != c.Repo {
		return false
	}

	if len(m.Labels) > 0 {
		found := true
		for k, v := range m.Labels {
			cv, ex := c.Labels[k]
			if !ex || cv != v {
				found = false
				break
			}
		}
		return found
	}
	return true
}

type ChangeHandler func(Change)
type ChangeSubscription struct {
	Match   ChangeMatch
	Handler ChangeHandler
}
