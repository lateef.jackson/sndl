package delta

import (
	"regexp"
	"testing"
)

type matchTest struct {
	cm    ChangeMatch
	c     Change
	match bool
}

func TestChangeMatch(t *testing.T) {
	tests := []matchTest{
		{
			cm: ChangeMatch{},
			c: Change{
				Name: "website-sndl",
			},
			match: true,
		},
		{
			cm: ChangeMatch{
				Name: regexp.MustCompile("website.*"),
			},
			c: Change{
				Name: "website-sndl",
			},
			match: true,
		},
		{
			cm: ChangeMatch{
				Name: regexp.MustCompile("website.*"),
			},
			c: Change{
				Name: "sndl-site",
			},
			match: false,
		},
		{
			cm: ChangeMatch{
				Repo: "sndl",
			},
			c: Change{
				Repo: "sndl",
			},
			match: true,
		},
		{
			cm: ChangeMatch{
				Repo: "sndl",
			},
			c: Change{
				Repo: "sndl",
			},
			match: true,
		},
		{
			cm: ChangeMatch{
				Labels: map[string]string{"key": "value"},
			},
			c: Change{
				Labels: map[string]string{"key": "value"},
			},
			match: true,
		},
		{
			cm: ChangeMatch{
				Labels: map[string]string{"key": "value"},
			},
			c: Change{
				Labels: map[string]string{"key": "test"},
			},
			match: false,
		},
	}

	for _, tt := range tests {
		if tt.cm.Match(&tt.c) != tt.match {
			t.Errorf("Expected Match to be %+v however it is %+v with test data %+v", tt.match, tt.cm.Match(&tt.c), tt)
		}
	}
}
