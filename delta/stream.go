package delta

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"regexp"
	"syscall"

	lua "github.com/yuin/gopher-lua"
)

type ChangeStream struct {
	Stream        chan Change
	Subscriptions []ChangeSubscription
	done          chan bool
}

func (cs *ChangeStream) Subscribe(sub ChangeSubscription) {
	cs.Subscriptions = append(cs.Subscriptions, sub)
}
func (cs *ChangeStream) Add(c Change) {
	cs.Stream <- c
}

func (cs *ChangeStream) Consume() {
	for {
		select {
		case c := <-cs.Stream:
			for _, sub := range cs.Subscriptions {
				if sub.Match.Match(&c) {
					sub.Handler(c)
				}
			}
		case <-cs.done:
			return
		}
	}
}
func (cs *ChangeStream) HandleHttpChange(w http.ResponseWriter, r *http.Request) {

	bits, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("Error read body bytes %s\n", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	c, err := UnmarshalChange(bits)
	if err != nil {
		log.Printf("Error decoding change json %s\n", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	fmt.Printf("Change is %+v\n", c)
	cs.Add(c)
	io.WriteString(w, "OK")
}

var (
	allChangeStreams []*ChangeStream
)

func init() {

	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		for _, c := range allChangeStreams {
			c.done <- true
		}
	}()
}

func NewChangeStream(buffSize int) *ChangeStream {
	cs := &ChangeStream{Stream: make(chan Change, buffSize), Subscriptions: []ChangeSubscription{}, done: make(chan bool, 1)}
	// TODO: Locking issues
	allChangeStreams = append(allChangeStreams, cs)
	return cs
}

const (
	changeStreamType = "deltaChangeStream"
	DeltaLibName     = "delta"
)

var changeFuncs = map[string]lua.LGFunction{}

var changeStreamFuncs = map[string]lua.LGFunction{
	"listen": listen,
	"handle": handle,
}

func newStream(L *lua.LState) int {
	cs := NewChangeStream(0)
	ud := L.NewUserData()
	ud.Value = cs
	L.SetMetatable(ud, L.GetTypeMetatable(changeStreamType))
	L.Push(ud)
	return 1
}

func checkChangeStream(L *lua.LState) *ChangeStream {
	ud := L.CheckUserData(1)
	if v, ok := ud.Value.(*ChangeStream); ok {
		return v
	}
	L.ArgError(1, "change stream not found")
	return nil
}

func ChangeToLuaTable(L *lua.LState, c Change) *lua.LTable {
	tbl := L.NewTable()
	tbl.RawSetString("key", lua.LString(c.Key))
	tbl.RawSetString("name", lua.LString(c.Name))
	tbl.RawSetString("type", lua.LString(c.Type))
	tbl.RawSetString("repo", lua.LString(c.Repo))
	tbl.RawSetString("start_time", lua.LNumber(c.StartTime.Unix()))
	tbl.RawSetString("end_time", lua.LNumber(c.EndTime.Unix()))
	tbl.RawSetString("settle_delay", lua.LString(c.SettleDelay.String()))
	tbl.RawSetString("description", lua.LString(c.Description))
	tbl.RawSetString("changeset", lua.LString(c.ChangeSet))
	lbls := L.NewTable()
	for k, v := range c.Labels {
		lbls.RawSetString(k, lua.LString(v))
	}
	tbl.RawSetString("labels", lbls)
	return tbl
}

func handle(L *lua.LState) int {
	stream := checkChangeStream(L)
	config := L.CheckTable(2)
	match := ChangeMatch{}

	name := config.RawGetString("name")
	if name != lua.LNil {
		match.Name = regexp.MustCompile(name.String())
	}
	repo := config.RawGetString("repo")
	if repo != lua.LNil {
		match.Repo = repo.String()
	}

	lbls := config.RawGetString("labels")
	if lbls.Type() != lua.LTNil {
		lbms := map[string]string{}
		lbls.(*lua.LTable).ForEach(func(k, v lua.LValue) {
			lbms[k.String()] = v.String()
		})
		match.Labels = lbms
	}
	handler := config.RawGetString("handler").(*lua.LFunction)
	cs := ChangeSubscription{
		Match: match,
		Handler: func(c Change) {
			tbl := ChangeToLuaTable(L, c)

			L.CallByParam(
				lua.P{
					Fn:   handler,
					NRet: 0,
				}, tbl)
		},
	}
	stream.Subscribe(cs)

	return 1
}

func Listen(stream *ChangeStream, host, path string) {
	mx := http.NewServeMux()

	mx.HandleFunc(path, stream.HandleHttpChange)
	fmt.Printf("host is '%s' and path is '%s'\n", host, path)
	httpServ := &http.Server{Addr: host, Handler: mx}
	log.Printf("Listing on host %s\n", host)
	err := httpServ.ListenAndServe()
	if err != nil {
		log.Printf("Error %s ListenAndServe on host %s\n", err, host)
	}
}

func listen(L *lua.LState) int {
	stream := checkChangeStream(L)
	go stream.Consume()
	host := L.CheckString(2)
	path := L.CheckString(3)
	Listen(stream, host, path)
	return 0
}

var deltaFuncs = map[string]lua.LGFunction{
	"stream": newStream,
}

func OpenDelta(L *lua.LState) int {
	mod := L.RegisterModule(DeltaLibName, deltaFuncs).(*lua.LTable)
	cst := L.NewTypeMetatable(changeStreamType)
	L.SetGlobal(changeStreamType, cst)
	cst.RawSetString("__index", cst)
	L.SetFuncs(cst, changeStreamFuncs)

	L.Push(mod)
	return 1
}
