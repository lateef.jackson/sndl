
Publish deploy message

```bash
curl -X POST 'http://localhost:7070/change' -H 'Content-Type: application/json' -d '{"name":"foo", "type": "deploy", "repo": "repo", "start_time": "2023-08-02T17:25:58Z", "end_time":"2023-08-02T17:36:07Z", "settle_delay": "15m", "labels": {"foo":"bar", "service":"website"}}'
```

This message will not match name

```bash

curl -X POST "http://localhost:7070/change" -H 'Content-Type: application/json' -d '{"name":"bar", "repo": "repo", "type": "deploy", "start_time": "2023-08-02T17:25:58Z", "end_time":"2023-08-02T17:36:07Z", "settle_delay": "15m", "labels": {"foo":"bar", "service":"website"}}'
```
