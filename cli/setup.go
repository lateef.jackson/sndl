package cli

import (
	"log"
	"os"
	"path/filepath"
)

const SNDL_FEET = `
     ⢀⣀⣀⣀⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⣀⣀⣀⡀⠀⠀⠀⠀⠀
⠀⠀⠀⣠⣾⣿⣿⡿⢿⣿⣿⣶⡄⠀⠀⠀⠀⢠⣶⣿⣿⡿⢿⣿⣿⣷⣄⠀⠀⠀
⠀⠀⣰⣿⣿⡿⠋⠀⣠⡄⠙⢿⣿⠀⠀⠀⠀⣿⡿⠋⢠⣄⠀⠙⢿⣿⣿⣆⠀⠀
⠀⢀⣿⣿⠏⢠⡞⠀⠛⠃⠐⣄⠙⡇⠀⠀⢸⠋⣠⠂⠘⠛⠀⢳⡄⠹⣿⣿⡀⠀
⠀⢸⡿⠃⣴⠋⣠⣾⣿⣿⣦⠘⣆⠀⠀⠀⠀⣰⠃⣴⣿⣿⣷⣄⠙⣦⠘⢿⡇⠀
⠀⠘⠁⡼⠁⣴⣿⣿⣿⣿⣿⡆⢸⡄⠀⠀⢠⡇⢰⣿⣿⣿⣿⣿⣦⠈⢧⠈⠃⠀
⠀⠀⣸⠃⣼⣿⣿⣿⣿⣿⣿⣷⠀⣧⠀⠀⣼⠀⣾⣿⣿⣿⣿⣿⣿⣧⠘⣇⠀⠀
⠀⠀⣿⠀⣿⣿⣿⣿⣿⣿⣿⡟⢀⡿⠀⠀⢿⡀⢻⣿⣿⣿⣿⣿⣿⣿⠀⣿⠀⠀
⠀⠀⠸⣇⠘⣿⣿⣿⣿⣿⣿⠁⣼⠃⠀⠀⠘⣧⠈⣿⣿⣿⣿⣿⣿⠃⣸⠇⠀⠀
⠀⠀⠀⠀⣤⣾⣿⣿⣿⣿⣷⣶⣶⠀⠀⠀⠀⣶⣶⣾⣿⣿⣿⣿⣷⣤⠀⠀⠀⠀
⠀⠀⠀⠹⡆⢸⣿⣿⣿⣿⡇⠸⠏⠀⠀⠀⠀⠹⠇⢸⣿⣿⣿⣿⡇⢰⠏⠀⠀⠀
⠀⠀⠀⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿⡀⠀⠀⢀⣿⣿⣿⣿⣿⣿⣿⣿⣿⠀⠀⠀⠀
⠀⠀⠀⠀⢻⣿⣿⣿⣿⣿⣿⣿⣿⡇⠀⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⡟⠀⠀⠀⠀
⠀⠀⠀⠀⠈⠻⣿⣿⣿⣿⣿⣿⡿⠀⠀⠀⠀⢿⣿⣿⣿⣿⣿⣿⠟⠁⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠈⠉⠉⠉⠁⠀⠀⠀⠀⠀⠀⠈⠉⠉⠉⠁⠀⠀⠀⠀⠀⠀⠀
`

var LuaPath string

func init() {
	LuaPath = os.Getenv("LUA_PATH")
	LuaPath = LuaPath + "./?.lua"
	wdp, err := os.Getwd()
	if err != nil {
		log.Printf("Failed to get working directory error: %s\n", err)
	} else {
		LuaPath = LuaPath + ";" + filepath.Dir(wdp) + "/?.lua"
	}
	os.Setenv("LUA_PATH", LuaPath)
}
