// There is a lot of copy pasta from https://github.com/yuin/gopher-lua/blob/master/iolib.go in this library
// Over time the code should get replaced based on layers of security. This is only copied because the ioFunctions
// have not been accessible from third party code.
package sndl

import (
	"bufio"
	"io"
	"os"

	lua "github.com/yuin/gopher-lua"
)

type PathAcl struct {
	Path  string `json:"path"`
	Read  bool   `json:"read"`
	Write bool   `json:"write"`
}
type IoAcl struct {
	Module string    `json:"module"`
	Allow  []PathAcl `json:"allow"`
}

type ioType int

const (
	ioFileType ioType = iota

	ioModule = "io"

	luaFileTypeName = "SNDL_FILE"
)

func OpenIO(L *lua.LState) int {
	mod := L.RegisterModule(ioModule, ioFunctions).(*lua.LTable)

	mt := L.NewTypeMetatable(luaFileTypeName)
	L.SetGlobal(luaFileTypeName, mt)
	mt.RawSetString("__index", mt)
	L.SetFuncs(mt, fileFunctions)

	L.Push(mod)
	return 1
}

type sndlFile struct {
	path     string
	gf       *os.File
	open     bool
	readable bool
	writable bool
	br       *bufio.Reader
	bw       io.Writer
}

func (sf *sndlFile) Type() ioType {
	return ioFileType
}

var ioFunctions = map[string]lua.LGFunction{

	"open":  ioOpen,
	"close": ioClose,
	"lines": ioLines,
	//"tmpfile": ioTmpFile,
	// "input":ioInput,
	// "output":ioOutput,
	// "flush": ioFlush,
	// "type": ioType,
	// "write": ioWrite,
}

func newFile(L *lua.LState, path string, flag int, perm os.FileMode, readable, writable bool) (*lua.LUserData, error) {
	ud := L.NewUserData()
	var err error

	var f *os.File
	f, err = os.OpenFile(path, flag, perm)

	if err != nil {
		return nil, err
	}
	var br *bufio.Reader
	if readable {
		br = bufio.NewReader(f)
	}
	var bw io.Writer
	if writable {
		bw = bufio.NewWriter(f)
	}
	ud.Value = &sndlFile{path: path, gf: f, open: true, readable: readable, writable: writable, br: br, bw: bw}
	L.SetMetatable(ud, L.GetTypeMetatable(luaFileTypeName))
	return ud, nil
}

// Copy pasta from: https://github.com/yuin/gopher-lua/blob/d212719c8304b1dfbe24bbc86780668ac612fc94/iolib.go#L616
var ioOpenOpions = []string{"r", "rb", "w", "wb", "a", "ab", "r+", "rb+", "w+", "wb+", "a+", "ab+"}

// Copy pasta with tweaks: https://github.com/yuin/gopher-lua/blob/d212719c8304b1dfbe24bbc86780668ac612fc94/iolib.go#L66
func ioOpen(L *lua.LState) int {
	path := L.CheckString(1)
	if L.GetTop() == 1 {
		L.Push(lua.LString("r"))
	}
	mode := os.O_RDONLY
	perm := 0600
	writable := true
	readable := true
	switch ioOpenOpions[L.CheckOption(2, ioOpenOpions)] {
	case "r", "rb":
		mode = os.O_RDONLY
		writable = false
	case "w", "wb":
		mode = os.O_WRONLY | os.O_TRUNC | os.O_CREATE
		readable = false
	case "a", "ab":
		mode = os.O_WRONLY | os.O_APPEND | os.O_CREATE
	case "r+", "rb+":
		mode = os.O_RDWR
	case "w+", "wb+":
		mode = os.O_RDWR | os.O_TRUNC | os.O_CREATE
	case "a+", "ab+":
		mode = os.O_APPEND | os.O_RDWR | os.O_CREATE
	}
	file, err := newFile(L, path, mode, os.FileMode(perm), readable, writable)
	if err != nil {
		L.Push(lua.LNil)
		L.Push(lua.LString(err.Error()))
		L.Push(lua.LNumber(1)) // C-Lua compatibility: Original Lua pushes errno to the stack
		return 3
	}
	L.Push(file)
	return 1
}

func ioClose(L *lua.LState) int {

	f := checkFile(L)
	if f.open {
		err := f.gf.Close()
		if err != nil {
			L.RaiseError(err.Error())
			f.br = nil
			f.open = false
			f.bw = nil
		}
	}
	return 0
}

func ioFlush(L *lua.LState) int {

	f := checkFile(L)
	if f.open && f.bw != nil {
		err := f.bw.(*bufio.Writer).Flush()
		if err != nil {
			L.RaiseError(err.Error())
		}
	}
	return 0
}

func checkFile(L *lua.LState) *sndlFile {
	ud := L.CheckUserData(1)
	if v, ok := ud.Value.(*sndlFile); ok {
		return v
	}
	L.ArgError(1, "sndlFile expected but not found")
	return nil
}

// Copy pasta from: https://github.com/yuin/gopher-lua/blob/d212719c8304b1dfbe24bbc86780668ac612fc94/iolib.go#L576
func ioLinesIter(L *lua.LState) int {
	var file *sndlFile
	toclose := false
	if ud, ok := L.Get(1).(*lua.LUserData); ok {
		file = ud.Value.(*sndlFile)
	} else {
		toclose = true
		file = L.Get(lua.UpvalueIndex(2)).(*lua.LUserData).Value.(*sndlFile)
	}
	buf, _, err := file.br.ReadLine()
	if err != nil {
		if err == io.EOF {
			if toclose {
				file.gf.Close()
			}

			L.Push(lua.LNil)
			return 1
		}
		L.RaiseError(err.Error())
	}
	L.Push(lua.LString(string(buf)))
	return 1
}

func ioLines(L *lua.LState) int {
	L.Push(L.Get(lua.UpvalueIndex(2)))
	/*if L.GetTop() == 0 {
		L.Push(fileDefIn(L))
		return 2
	}*/

	path := L.CheckString(1)
	ud, err := newFile(L, path, os.O_RDONLY, os.FileMode(0600), true, false)
	if err != nil {
		return 0
	}
	L.Push(L.NewClosure(ioLinesIter, L.Get(lua.UpvalueIndex(1)), ud))
	return 1
}

var fileFunctions = map[string]lua.LGFunction{
	"__tostring": fileToString,
	/*"write":      fileWrite,
	"read":       fileRead,
	"seek":       fileSeek,
	"flush":      fileFlush,*/
}

func fileToString(L *lua.LState) int {
	file := checkFile(L)
	if file.Type() == ioFileType {
		if file.open {
			L.Push(lua.LString("open file"))
		} else {
			L.Push(lua.LString("closed file"))
		}
	}
	return 1
}
