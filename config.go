package sndl

import (
	"encoding/json"
	"io"
)

const SndlConfigKey = "sndlConfig"

type Config struct {
	// TODO: Add modules configuration like Modules[map].....??
	Sockets    map[string]SocketAcl     `json:"sockets"`
	HttpServer map[string]HttpServerAcl `json:"httpservers"`
}

func ParseConfig(data io.Reader) (*Config, error) {
	bits, err := io.ReadAll(data)
	if err != nil {
		return nil, err
	}
	var cfg Config
	err = json.Unmarshal(bits, &cfg)
	return &cfg, err
}
