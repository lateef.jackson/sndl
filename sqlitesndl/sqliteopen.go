// The implementation was heavily inspired by: https://github.com/tengattack/gluasql/tree/master/sqlite3
package sqlitesndl

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	_ "github.com/mattn/go-sqlite3"
	lua "github.com/yuin/gopher-lua"
	"gitlab.com/lateef.jackson/sndl/dsl"
)

var sqliteFunctions = map[string]lua.LGFunction{
	"new": newDb,
}

const (
	Sqlite3Module         = "sqlite3"
	sqlite3ClientTypeName = "sqlite3Db"
	sqlite3RowsTypeName   = "sqlite3Rows"
)

func OpenSqlite3(L *lua.LState) int {
	mod := L.RegisterModule(Sqlite3Module, sqliteFunctions).(*lua.LTable)

	mt := L.NewTypeMetatable(sqlite3ClientTypeName)
	L.SetGlobal(sqlite3ClientTypeName, mt)
	mt.RawSetString("__index", mt)
	L.SetFuncs(mt, dbFunctions)

	sqlr := L.NewTypeMetatable(sqlite3RowsTypeName)
	L.SetGlobal(sqlite3RowsTypeName, sqlr)
	sqlr.RawSetString("__index", sqlr)
	L.SetFuncs(sqlr, sqlrFuncs)

	L.Push(mod)
	return 1
}

type sqliteClient struct {
	db       *sql.DB
	qt       time.Duration
	readable bool
	writable bool
}

type sqliteRows struct {
	columns []string
	rows    [][]interface{}
}

var sqlrFuncs = map[string]lua.LGFunction{
	/*"rows":    queryRows,
	"columns": queryColumns,*/
}

func newDb(L *lua.LState) int {
	c := &sqliteClient{}
	ud := L.NewUserData()
	ud.Value = c
	L.SetMetatable(ud, L.GetTypeMetatable(sqlite3ClientTypeName))
	L.Push(ud)
	fmt.Printf("Ok create sql connection %+v\n", c)
	return 1
}

func checkClient(L *lua.LState) *sqliteClient {
	ud := L.CheckUserData(1)
	if v, ok := ud.Value.(*sqliteClient); ok {
		return v
	}
	L.ArgError(1, "client not found")
	return nil
}

func connect(L *lua.LState) int {
	client := checkClient(L)
	path := L.ToString(2)

	ml := L.ToInt(3)

	var err error
	client.db, err = sql.Open("sqlite3", path)
	if err != nil {
		L.Push(lua.LBool(false))
		L.Push(lua.LString(err.Error()))
		return 2
	}
	if ml > 0 {
		client.qt = time.Duration(ml) * time.Second

	}
	L.Push(lua.LBool(true))
	return 1
}

func dbQuery(L *lua.LState) int {
	var ctx context.Context
	client := checkClient(L)
	if client.qt.Abs().Nanoseconds() > 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(context.Background(), client.qt)
		defer cancel()
	}
	q := L.ToString(2)

	var opts map[string]interface{}
	params := dsl.ValueToGoInterface(L, L.Get(3))
	if params != nil {
		opts = params.(map[string]interface{})
	}
	//fields, fieldsExists := opts["fields"]
	// Timeout for the query
	qt, qtExists := opts["timeout"]
	if qtExists {
		t := qt.(int)
		if t > 0 {
			var cancel context.CancelFunc
			ctx, cancel = context.WithTimeout(context.Background(), client.qt)
			defer cancel()
		}
	}
	if ctx == nil {
		ctx = context.Background()
	}
	stmt, err := client.db.PrepareContext(ctx, q)
	if err != nil {
		L.Push(lua.LNil)
		L.Push(lua.LString(err.Error()))
	}
	rows, err := stmt.Query()
	if err != nil {
		L.Push(lua.LNil)
		L.Push(lua.LString(err.Error()))
	}
	cols, _ := rows.Columns()
	resp := L.NewTable()
	for rows.Next() {
		pts := make([]interface{}, len(cols))
		columns := make([]string, len(cols))
		for i := range columns {
			pts[i] = &columns[i]
		}
		err := rows.Scan(pts...)
		if err != nil {
			L.Push(lua.LNil)
			L.Push(lua.LString(err.Error()))
		}
		m := map[string]interface{}{}
		for i, colName := range cols {
			m[colName] = columns[i]

		}
		//fmt.Printf("Scanned response is %+v\n", m)

		resp.Append(dsl.MapToTable(L, m))
	}
	L.Push(resp)
	L.Push(lua.LNil)
	return 2
}

var dbFunctions = map[string]lua.LGFunction{
	"query":   dbQuery,
	"connect": connect,
	//"close": dbClose,
}
