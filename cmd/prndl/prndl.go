package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"gitlab.com/lateef.jackson/sndl"
	"gitlab.com/lateef.jackson/sndl/cli"
	cdelta "gitlab.com/lateef.jackson/sndl/delta"
	"gitlab.com/lateef.jackson/sndl/lstate"
	"gitlab.com/lateef.jackson/sndl/prom"
)

func handleWindowConfig(ctx context.Context, client *prom.PromClient, cfg *prom.PromCfg) error {
	//fmt.Printf("Window config %+v\n", cfg)
	w := "5m"
	if cfg.Window != "" {
		w = cfg.Window
	}
	window, err := time.ParseDuration(w)
	if err != nil {
		log.Printf("Failed to parse Window in configuration")
		return err
	}
	windowTimeout := time.NewTicker(window)
	for {
		select {
		case <-windowTimeout.C:
			L := lstate.BuildDefaultLuaState(ctx)
			fmt.Printf("Loading window data for %+v\n", cfg)
			exps, err := prom.LoadData(L, ctx, client, cfg)
			if err != nil {
				log.Printf("Error running YML %s\n", err)
			}
			for k, v := range exps {
				fmt.Printf("Window Exp %s value is %+v\n", k, v)
			}
			L.Close()
			//fmt.Printf("Done string %s waiting for window %+v\n", window, windowTimeout)
		}
	}
}

func handleChangeConfig(ctx context.Context, client *prom.PromClient, cs *cdelta.ChangeStream, cfg *prom.PromCfg) error {

	match := cdelta.ChangeMatch{}
	if cfg.Match.Type != "" {
		match.Type = cfg.Match.Type
	}
	if cfg.Match.Repo != "" {
		match.Repo = cfg.Match.Repo
	}
	if len(cfg.Match.Labels) > 0 {
		match.Labels = cfg.Match.Labels
	}
	w := "5m"
	if cfg.Window != "" {
		w = cfg.Window
	}
	window, err := time.ParseDuration(w)
	if err != nil {
		log.Printf("Failed to parse Window in configuration")
		return err
	}

	cs.Subscribe(cdelta.ChangeSubscription{

		Match: match,
		Handler: func(c cdelta.Change) {
			go func() {
				fmt.Printf("Change recieved %+v\n", c)
				preStart := c.StartTime.Add(-window)
				preEnd := c.StartTime
				postStart := c.EndTime.Add(c.SettleDelay)
				postEnd := postStart.Add(window)

				L := lstate.BuildDefaultLuaState(ctx)
				defer L.Close()
				exps, err := prom.LoadChangeData(L, ctx, client, cfg, prom.NewWindow(preStart, preEnd), prom.NewWindow(postStart, postEnd))
				if err != nil { // TODO: Retry or figure out a way to not consume change
					log.Printf("Error running YML %s\n", err)

				}
				for k, v := range exps {
					fmt.Printf("Change exp %s value is %+v\n", k, v)
				}
			}()
		},
	})
	fmt.Println("Done subscribing...")
	return nil
}

func main() {
	var cfg *sndl.Config
	var configPath string
	flag.StringVar(&configPath, "cfg", "", "configuration file")
	flag.Parse()

	if configPath != "" {
		_, err := os.Stat(configPath)
		if err == nil {
			f, err := os.Open(configPath)
			if err != nil {
				log.Fatalf("Could not open file '%s' with error %s\n", configPath, err)
			}
			defer f.Close()
			cfg, err = sndl.ParseConfig(f)
			if err != nil {
				log.Fatalf("Could not parse config '%s' with error %s\n", configPath, err)
			}
		}
	}
	if configPath == "" {
		cfg = &sndl.Config{}
	}
	ctx := context.WithValue(context.Background(), sndl.SndlConfigKey, cfg)
	if len(os.Args) < 1 {
		log.Println("No path in argument....")
		os.Exit(1)
	}
	filePath := flag.Arg(0)

	if filePath == "" {
		log.Println("No configuration path to files as first argument ")
		os.Exit(1)
	}

	cfgs, err := prom.LoadFromPaths(filePath)
	if err != nil {
		log.Fatalf("Error: %s", err)
	}
	fmt.Print(cli.SNDL_FEET)

	// TODO: Read in from configuration
	host := "http://localhost:9090/"
	pc, err := prom.NewPromClient(host)
	if err != nil {
		fmt.Printf("Error connecting to prom client %s\n", err)
		return
	}
	cs := cdelta.NewChangeStream(1)
	for n, c := range cfgs.Configs {
		fmt.Printf("Name: %s with config %+v\n", n, c)
		switch c.Type {
		case string(prom.WindowConfig):
			go handleWindowConfig(ctx, pc, c)
		case string(prom.ChangeConfig):
			go handleChangeConfig(ctx, pc, cs, c)
		default:
			log.Printf("ERROR: failed to %+v\n", c)
		}
	}

	fmt.Println("Waiting ...")
	go cs.Consume()
	cdelta.Listen(cs, ":7070", "/change")
}
