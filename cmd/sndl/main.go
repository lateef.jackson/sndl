package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/c-bata/go-prompt"
	"github.com/fsnotify/fsnotify"

	lua "github.com/yuin/gopher-lua"
	"gitlab.com/lateef.jackson/sndl"
	sl "gitlab.com/lateef.jackson/sndl"
	"gitlab.com/lateef.jackson/sndl/cli"
	"gitlab.com/lateef.jackson/sndl/lstate"
	"gitlab.com/lateef.jackson/sndl/prom"
)

func main() {
	var cfg *sndl.Config
	var configPath string
	var reloadOnChange bool
	flag.StringVar(&configPath, "cfg", "", "configuration file")
	flag.BoolVar(&reloadOnChange, "reload", false, "Reload on file change")
	flag.Parse()

	envCfg := os.Getenv("SNDL_CFG")
	if envCfg != "" {
		configPath = envCfg
	}
	if configPath != "" {
		_, err := os.Stat(configPath)
		if err == nil {
			f, err := os.Open(configPath)
			if err != nil {
				log.Fatalf("Could not open file '%s' with error %s\n", configPath, err)
			}
			defer f.Close()
			cfg, err = sndl.ParseConfig(f)
			if err != nil {
				log.Fatalf("Could not parse config '%s' with error %s\n", configPath, err)
			}
		}
	}
	if configPath == "" {
		cfg = &sndl.Config{}
	}
	ctx := context.WithValue(context.Background(), sndl.SndlConfigKey, cfg)
	if len(os.Args) < 1 {
		log.Println("No path in argument....")
		os.Exit(1)
	}
	filePath := flag.Arg(0)
	if filePath == "" {
		ctx = context.WithValue(ctx, sndl.CallerModuleKey, "repl")
		L := lstate.BuildDefaultLuaState(ctx)
		defer L.Close()
		fmt.Print(cli.SNDL_FEET)
		// Run repl
		cr := codeRunner{L: L}
		p := prompt.New(
			cr.executor,
			cr.completer,
			prompt.OptionPrefix(PROMPT),
			prompt.OptionLivePrefix(cr.livePrefix),
			prompt.OptionTitle("sndl"),
		)

		p.Run()
	} else {

		fmt.Printf("File path %s\n", filePath)
		baseDir, fileName := filepath.Split(filePath)
		fmt.Printf("basedir %s file name %s\n", baseDir, fileName)
		fileName = strings.Split(fileName, ".")[0]
		fmt.Printf("File name is: %s\n", fileName)
		// creates a new file watcher
		watcher, err := fsnotify.NewWatcher()
		if err != nil {
			fmt.Println("ERROR", err)
		}
		watcher.Add(filePath)
		watcher.Add(baseDir)
		defer watcher.Close()

		luaPath := cli.LuaPath + ";" + baseDir + "/?.lua"
		os.Setenv("LUA_PATH", luaPath)
		//
		done := make(chan bool)
		ctx = context.WithValue(ctx, sndl.CallerModuleKey, fileName)

		cctx, cancel := context.WithCancel(ctx)
		L := lstate.BuildDefaultLuaState(cctx)
		if reloadOnChange {
			go runLuaFile(L, filePath)
			//
			go func() {
				for {
					select {
					// watch for events
					case event, ok := <-watcher.Events:
						if !ok {
							return
						}
						fmt.Printf("Op %s file %s\n", event.Op.String(), event.Name)
						if event.Has(fsnotify.Write) || event.Has(fsnotify.Create) {
							//fmt.Printf("EVENT! %#v\n", event)
							cancel()
							// Create a new state and run it
							sl.ShutdownHttp()
							cctx, cancel = context.WithCancel(ctx)
							L = lstate.BuildDefaultLuaState(cctx)
							go runLuaFile(L, filePath)
						}
						// watch for errors
					case err := <-watcher.Errors:
						log.Fatalf("ERROR %s\n", err)
					}
				}
			}()

			<-done
		} else {
			L := lstate.BuildDefaultLuaState(ctx)
			defer L.Close()
			runLuaFile(L, filePath)
		}

	}
}

func runLuaFile(L *lua.LState, filePath string) {
	fmt.Printf("Running %s\n", filePath)
	err := L.DoFile(filePath)
	if err != nil && err != context.Canceled {
		fmt.Printf("Error running file: %s\n", err)
	}
}

const PROMPT = "(->): "

type codeRunner struct {
	L          *lua.LState
	PromClient *prom.PromClient
}

func contains(ms []string, m string) bool {
	for _, k := range ms {
		if strings.HasPrefix(m, k) {
			return true
		}
	}
	return false
}
func (cr *codeRunner) completer(d prompt.Document) []prompt.Suggest {
	sgs := []prompt.Suggest{}
	//fmt.Printf("%+v\n", d)
	//fmt.Printf("Word before cursor '%s' after cusor '%s'\n", d.GetWordBeforeCursor(), d.GetWordAfterCursor())
	words := strings.Split(d.Text, "(")
	if len(words) > 0 {
		name := ""
		if strings.HasPrefix(words[0], prom.METRIC) && len(words) > 1 {
			name = strings.TrimSpace(strings.Trim(words[1], "'"))
		}
		if cr.PromClient != nil {
			ms, err := cr.PromClient.MetricNames("")
			if err != nil {
				fmt.Printf("Error getting metricNames %s\n", err)
				return sgs
			}
			if name != "" && contains(ms, name) {
				lbs, err := cr.PromClient.MetricLabels(name)
				if err != nil {
					fmt.Printf("Error getting labels: %s\n", err)
					return sgs
				}
				for _, l := range lbs {
					sgs = append(sgs, prompt.Suggest{Text: l + "='", Description: "Label: " + l})
				}
				sgs = prompt.FilterContains(sgs, strings.Trim(d.GetWordBeforeCursor(), "'"), true)
			} else if strings.HasPrefix(words[0], prom.METRIC) {
				for _, k := range ms {
					sgs = append(sgs, prompt.Suggest{Text: "'" + k + "',", Description: "Metric: " + k})
				}
				if len(words) > 1 {
					sgs = prompt.FilterContains(sgs, strings.Trim(d.GetWordBeforeCursor(), "'"), true)
				}
			} else {
				for k, v := range prom.PqlDescriptions {
					sgs = append(sgs, prompt.Suggest{Text: k + "(", Description: v})
				}
			}
		}
	}
	//return prompt.FilterContains(sgs, word, true)
	return sgs
}

func (cr *codeRunner) livePrefix() (string, bool) {
	if cr.PromClient != nil {
		return "pql " + PROMPT, true
	}
	return PROMPT, true
}
func (cr *codeRunner) executor(line string) {
	//fmt.Printf("Loading string '%s'\n", line)
	if strings.HasPrefix(line, "pql") {

		words := strings.Split(strings.TrimSpace(line), " ")
		host := "http://localhost:9090/"
		if len(words) > 1 {
			host = words[2]
		}
		pc, err := prom.NewPromClient(host)
		if err != nil {
			fmt.Printf("Error connecting to prom client %s\n", err)
			return
		}
		cr.PromClient = pc

		// Set some globals
		global := cr.L.Get(lua.GlobalsIndex).(*lua.LTable)
		global.RawSetString("metric", cr.L.NewFunction(cr.PromClient.Metric))
		return
	}
	// Check if prom query
	if cr.PromClient != nil {

	}
	_, err := cr.L.LoadString("return " + line)
	if err != nil {
		_, err = cr.L.LoadString(line)
		if err != nil {

			fmt.Printf("So sad so bad LoadString error: '%s'", err)
			fmt.Printf("Doing string '%s'\n", line)
		}
	}
	err = cr.L.DoString(line)
	if err != nil {
		fmt.Printf("So sad so bad DoString error: '%s'", err)
	}
}
