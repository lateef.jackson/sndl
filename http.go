package sndl

import (
	"context"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	lua "github.com/yuin/gopher-lua"
)

type httpType int

const (
	HttpLibName              = "http"
	httpRequestType httpType = iota
	httpResponseType

	httpModule = "http"

	luaHttpRequestTypeName  = "httpRequest"
	luaHttpResponseTypeName = "httpResponse"
)

type RequestAcl struct {
	Form      bool `json:"form"`
	Multipart bool `json:"multipart"`
	Body      bool `json:"body"`
}

type ResponseAcl struct {
	Read  bool `json:"read"`
	Write bool `json:"write"`
}

type ListenAcl struct {
	Host string `json:"host"`
	Port int    `json:"port"`
}

type HttpServerAcl struct {
	Package  string      `json:"package"`
	Request  RequestAcl  `json:"request"`
	Resposne ResponseAcl `json:"response"`
	Listen   ListenAcl   `json:"listen"`
}

func findHttpServerAcl(L *lua.LState) *HttpServerAcl {
	cfg := getConfig(L)
	if cfg != nil {
		s, exists := cfg.HttpServer[getCallingPackage(L)]
		if exists {
			return &s
		}
	}
	return nil
}

type request struct {
	*http.Request
}

func checkRequest(L *lua.LState) *request {
	ud := L.CheckUserData(1)
	if v, ok := ud.Value.(*request); ok {
		return v
	}
	L.ArgError(1, "request expected")
	return nil
}

var httpRequestFuncs = map[string]lua.LGFunction{}

type response struct {
	http.ResponseWriter
}

func checkResponse(L *lua.LState) *response {
	ud := L.CheckUserData(1)
	if v, ok := ud.Value.(*response); ok {
		return v
	}
	L.ArgError(1, "response expected")
	return nil
}

func write(L *lua.LState) int {
	resp := checkResponse(L)
	hs := findHttpServerAcl(L)
	if hs == nil {
		m := getCallingPackage(L)
		msg := fmt.Sprintf("package: '%s' is not configured for http response", m)
		log.Println(msg)
		L.Push(lua.LString(msg))
		return 1
	}
	if !hs.Resposne.Write {
		m := getCallingPackage(L)
		msg := fmt.Sprintf("package: '%s' response is not configured to be writable", m)
		log.Println(msg)
		L.Push(lua.LString(msg))
		return 1
	}
	contents := L.CheckString(2)
	total, err := io.WriteString(resp, contents)
	L.Push(lua.LNumber(total))
	if err != nil {
		L.Push(lua.LString(err.Error()))
		return 2
	}
	L.Push(lua.LNil)
	return 2
}
func status(L *lua.LState) int {
	resp := checkResponse(L)
	hs := findHttpServerAcl(L)
	if hs == nil {
		m := getCallingPackage(L)
		msg := fmt.Sprintf("package: '%s' is not configured for http response", m)
		log.Println(msg)
		L.Push(lua.LString(msg))
		return 1
	}
	if !hs.Resposne.Write {
		m := getCallingPackage(L)
		msg := fmt.Sprintf("package: '%s' response is not configured to be writable", m)
		log.Println(msg)
		L.Push(lua.LString(msg))
		return 1
	}
	stat := L.CheckInt(2)
	resp.WriteHeader(stat)
	return 0
}

var httpResponseFuncs = map[string]lua.LGFunction{
	"write":  write,
	"status": status,
}

var (
	httpServ   *http.Server
	defaultMux *http.ServeMux
)

func init() {
	defaultMux = http.NewServeMux()
}

// XXX: Silent failure if the request is not configured. This may not be the best approach it would be nice to make the failure louder and stronger.
func wrapRequest(L *lua.LState, r *http.Request) lua.LValue {
	hs := findHttpServerAcl(L)
	if hs == nil {
		m := getCallingPackage(L)
		msg := fmt.Sprintf("package: '%s' is not configured for http request", m)
		log.Println(msg)
		return lua.LString(msg)
	}
	reqTable := L.NewTable()
	reqTable.RawSetString("method", lua.LString(r.Method))
	reqTable.RawSetString("path", lua.LString(r.URL.Path))
	reqTable.RawSetString("host", lua.LString(r.Host))
	reqTable.RawSetString("query", lua.LString(r.URL.RawQuery))
	reqTable.RawSetString("uri", lua.LString(r.RequestURI))
	reqTable.RawSetString("address", lua.LString(r.RemoteAddr))
	hdrTbl := L.NewTable()
	for k, v := range r.Header {
		vlsTbl := L.NewTable()
		for _, h := range v {
			vlsTbl.Append(lua.LString(h))
		}
		hdrTbl.RawSetString(k, vlsTbl)
	}
	reqTable.RawSetString("header", hdrTbl)
	if hs.Request.Form {
		r.ParseForm()
		frmTbl := L.NewTable()
		for k, vls := range r.Form {
			fvs := L.NewTable()
			for _, v := range vls {
				fvs.Append(lua.LString(v))
			}
			frmTbl.RawSetString(k, fvs)
		}
		reqTable.RawSetString("form", frmTbl)
	}
	// TODO: Implement multipart form
	if hs.Request.Multipart {
		log.Println("Multipart is not implemented please create a ticker or whatever....")
	}

	if hs.Request.Body {
		reqTable.RawSetString("body", L.NewFunction(func(state *lua.LState) int {
			defer r.Body.Close()
			// TODO: Add upper bound on request body
			// Memory is cheep why not ?
			b, err := io.ReadAll(r.Body)
			if err != nil {
				L.Push(lua.LNil)
				L.Push(lua.LString("Error reading request body: " + err.Error()))
			}
			L.Push(lua.LString(string(b)))
			L.Push(lua.LNil)
			return 2
		}))
	}

	return reqTable
}

func wrapResponse(L *lua.LState, w http.ResponseWriter) *lua.LUserData {
	resp := &response{ResponseWriter: w}
	ud := L.NewUserData()
	ud.Value = resp
	L.SetMetatable(ud, L.GetTypeMetatable(luaHttpResponseTypeName))
	return ud
}

type responseWrapper struct {
	http.ResponseWriter
	status int
}

func (w *responseWrapper) WriteHeader(status int) {

	w.status = status
	w.ResponseWriter.WriteHeader(status)
}

func (w *responseWrapper) Write(b []byte) (int, error) {
	return w.ResponseWriter.Write(b)
}

func match(L *lua.LState) int {

	path := L.CheckString(1)
	fnc := L.CheckFunction(2)
	httpRespsGauge := promauto.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: "sndl",
			Subsystem: "httphandler",
			Name:      "sndl_http_response_seconds",
			Help:      "sndl match http response time for all requests",
		},
		[]string{
			"code", "method", "path",
		},
	)
	httpReqs := promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "sndl_http_match_request",
		Help: "The total number of match http requests",
	}, []string{"code", "method", "path"})
	defaultMux.HandleFunc(path, func(w http.ResponseWriter, r *http.Request) {
		rw := &responseWrapper{ResponseWriter: w}
		req := wrapRequest(L, r)
		resp := wrapResponse(L, rw)
		start := time.Now()
		defer func() {
			httpRespsGauge.WithLabelValues(strconv.Itoa(rw.status), r.Method, r.URL.Path).Add(time.Until(start).Seconds())
			httpReqs.WithLabelValues(strconv.Itoa(rw.status), r.Method, r.URL.Path).Inc()
		}()
		L.CallByParam(lua.P{
			Fn:   fnc,
			NRet: 0,
		}, resp, req)
	})
	log.Printf("Registered path %s\n", path)
	return 0
}

func ShutdownHttp() {
	// Reset the default mux and make sure http server is stopped
	httpServ.Shutdown(context.Background())
	defaultMux = http.NewServeMux()
}

func publishMetrics(L *lua.LState) int {
	path := "/metrics"
	if L.CheckAny(1) != lua.LNil {
		path = L.CheckString(1)
	}
	defaultMux.Handle(path, promhttp.Handler())
	return 0

}

func listen(L *lua.LState) int {

	hs := findHttpServerAcl(L)
	if hs == nil {
		callingPackage := getCallingPackage(L)
		msg := fmt.Sprintf("package: '%s' is not configured for http request", callingPackage)
		log.Println(msg)
		L.Push(lua.LString(msg))
		return 1
	}
	host := L.CheckString(1)
	if hs.Listen.Host != host {
		callingPackage := getCallingPackage(L)
		msg := fmt.Sprintf("package: '%s' is not configured for listening to host %s", callingPackage, host)
		log.Println(msg)
		L.Push(lua.LString(msg))
		return 1

	}
	httpServ = &http.Server{Addr: host, Handler: defaultMux}
	log.Printf("Listing on host %s\n", host)
	err := httpServ.ListenAndServe()
	if err != nil {
		msg := fmt.Sprintf("Error %s ListenAndServe on host %s\n", err, host)
		log.Print(msg)
		L.Push(lua.LString(msg))
		return 1
	}
	L.Push(lua.LNil)
	return 1
}

var httpFuncs = map[string]lua.LGFunction{
	"match":           match,
	"listen":          listen,
	"publish_metrics": publishMetrics,
}

func OpenHttp(L *lua.LState) int {
	mod := L.RegisterModule(httpModule, httpFuncs).(*lua.LTable)

	reqMt := L.NewTypeMetatable(luaHttpRequestTypeName)
	L.SetGlobal(luaHttpRequestTypeName, reqMt)
	reqMt.RawSetString("__index", reqMt)
	L.SetFuncs(reqMt, httpRequestFuncs)

	respMt := L.NewTypeMetatable(luaHttpResponseTypeName)
	L.SetGlobal(luaHttpResponseTypeName, respMt)
	respMt.RawSetString("__index", respMt)
	L.SetFuncs(respMt, httpResponseFuncs)

	L.Push(mod)
	return 1
}
