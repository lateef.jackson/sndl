package prom

import (
	"testing"
	"time"
)

func TestDiff(t *testing.T) {

	d1 := map[string][]Sample{
		"host1": {
			{
				Timestamp: time.Time{}.Add(1 * time.Second),
				Value:     0,
			},
			{
				Value:     0,
				Timestamp: time.Time{}.Add(1 * time.Second),
			},
		}}

	d2 := map[string][]Sample{
		"host1": {
			{
				Timestamp: time.Time{}.Add(1 * time.Second),
				Value:     1,
			},
			{
				Value:     1,
				Timestamp: time.Time{}.Add(1 * time.Second),
			},
		}}

	diff1 := Diff(d1, d2)
	if len(diff1) != 1 {
		t.Fatal("Expected on instance in diff")
	}
	if diff1["host1"][0].Value != 1 {
		t.Fatalf("Expected 1 value diff got %0.2f", diff1["host1"][0].Value)
	}

}
