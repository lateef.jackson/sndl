package prom

import (
	"context"
	"fmt"
	"log"
	"regexp"
	"time"

	"github.com/prometheus/client_golang/api"
	v1 "github.com/prometheus/client_golang/api/prometheus/v1"
	"github.com/prometheus/common/model"
	lua "github.com/yuin/gopher-lua"
	cdelta "gitlab.com/lateef.jackson/sndl/delta"
	"gitlab.com/lateef.jackson/sndl/dsl"
)

const (
	METRIC = "metric"
)

var PqlDescriptions = map[string]string{
	METRIC: "Filter / query metric",
}

// Type alias
type MetricData map[string][]Sample

type Client interface {
	RangeQuery(qs QueryStatement, start time.Time, end time.Time, step string) (MetricData, error)
}
type PromClient struct {
	Client api.Client
	Api    v1.API
}

func NewPromClient(addr string) (*PromClient, error) {
	c, err := api.NewClient(api.Config{Address: addr})
	if err != nil {
		return nil, err
	}
	return &PromClient{Client: c, Api: v1.NewAPI(c)}, nil
}

func (pc *PromClient) MetricNames(match string) ([]string, error) {
	meta, err := pc.Api.Metadata(context.Background(), "", "1000")
	if err != nil {
		return nil, err
	}
	metrics := []string{}
	for m := range meta {
		//fmt.Printf("Metric %s value %+v\n", m, v)
		metrics = append(metrics, m)
	}
	return metrics, nil
}
func (pc *PromClient) MetricLabels(name string) ([]string, error) {

	vls, wrns, err := pc.Api.LabelNames(context.Background(), []string{name}, time.Time{}, time.Time{})
	if err != nil {
		return nil, err
	}
	if len(wrns) > 0 {
		for _, w := range wrns {
			fmt.Printf("Label query wrn %+v", w)
		}
	}
	return vls, nil
}

func (pc *PromClient) Query(q string, ts time.Time) error {
	_, wrns, err := pc.Api.Query(context.Background(), q, ts)
	for w := range wrns {
		fmt.Printf("Promql Warning: %+v\n", w)
	}
	if err != nil {
		return err
	}
	return nil
}

type queryRange struct {
	start time.Time
	end   time.Time
	step  string
}

func buildQueryStatment(index int, L *lua.LState) (QueryStatement, *queryRange) {
	now := time.Now()

	name := L.CheckString(index)
	qs := QueryStatement{MetricName: name}
	qr := &queryRange{
		start: now.Add(-10 * time.Minute),
		end:   time.Now(),
		step:  "10s",
	}
	fmt.Printf("Query is %s\n", qs.String())
	lbms := []LabelMatch{}
	if L.ToTable(index+1) != nil {
		config := L.CheckTable(index + 1)

		duration := config.RawGetString("duration")
		if duration == lua.LNil {
			duration = lua.LString("1m")
		}
		ds := duration.(lua.LString)
		d, err := time.ParseDuration(ds.String())
		if err != nil {
			fmt.Printf("Failed to parse duration %s\n", err)
		} else {
			qr.start = now.Add(-d)
		}

		offset := config.RawGetString("offset")
		if offset != lua.LNil {
			offset = lua.LString("1m")
			ofs := duration.(lua.LString)
			o, err := time.ParseDuration(ofs.String())
			if err != nil {
				fmt.Printf("Failed to parse duration %s\n", err)
			} else {
				qs.Offset = o
			}
		}
		rng := config.RawGetString("range")
		if rng != lua.LNil && rng.Type() == lua.LTTable {
			st := rng.(*lua.LTable).RawGetString("start_time")
			start := dsl.ToGoTime(st.(lua.LNumber))
			en := rng.(*lua.LTable).RawGetString("end_time")
			end := dsl.ToGoTime(en.(lua.LNumber))
			qr = &queryRange{
				start: start,
				end:   end,
			}
			stp := rng.(*lua.LTable).RawGetString("step")
			if stp != lua.LNil && stp.String() != "" {
				qr.step = stp.String()
			}
		}

		lbls := config.RawGetString("labels")
		if lbls.Type() != lua.LTNil {
			lbls.(*lua.LTable).ForEach(func(k, v lua.LValue) {
				lbms = append(lbms, LabelMatch{Label: k.String(), Value: v.String(), Op: EqToken})
			})
		}
	}
	qs.Labels = lbms
	return qs, qr
}
func (pc *PromClient) RangeQuery(qs QueryStatement, start time.Time, end time.Time, step string) (MetricData, error) {

	//fmt.Printf("Query: %s\n", qs.String())
	//fmt.Printf("Start: %s End: %s Step: %s\n", start, end, step)
	var d time.Duration
	var err error
	if step != "" {
		d, err = time.ParseDuration(step)
		if err != nil {
			return nil, err
		}
	}
	vls, _, err := pc.Api.QueryRange(context.Background(), qs.String(), v1.Range{
		Start: start,
		End:   end,
		Step:  d,
	})
	if err != nil {
		return nil, err
	}

	mtx, ok := vls.(model.Matrix)
	if !ok {
		return nil, fmt.Errorf("expected type model.Matrix however type is %T", vls)
	}

	return matrixToMap(mtx), nil
}

func (pc *PromClient) QueryRange(qs QueryStatement, start time.Time, end time.Time, step string) (model.Value, v1.Warnings, error) {
	//fmt.Printf("Query: %s\n", qs.String())
	//fmt.Printf("Start: %s End: %s Step: %s\n", start, end, step)
	var d time.Duration
	var err error
	if step != "" {
		d, err = time.ParseDuration(step)
		if err != nil {
			return nil, nil, err
		}
	}
	return pc.Api.QueryRange(context.Background(), qs.String(), v1.Range{
		Start: start,
		End:   end,
		Step:  d,
	})
}
func PromResultsToLua(L *lua.LState, vls model.Value) (lua.LValue, lua.LValue) {
	//fmt.Printf("values %s\n", vls)
	//fmt.Printf("Values Type: %T\n", vls)
	t := vls.Type()
	switch t {
	case model.ValNone:
		return lua.LNil, lua.LNil
	case model.ValString:
		return lua.LString(vls.(*model.String).String()), lua.LNil
	case model.ValScalar:
		return lua.LNumber(vls.(*model.Scalar).Value), lua.LNil
	case model.ValVector:
		vect := vls.(model.Vector)
		//fmt.Printf("Vector %+v\n", vect)
		tbl := L.NewTable()
		for _, v := range vect {
			tbl.Append(lua.LNumber(v.Value))
		}
		return tbl, lua.LNil
	case model.ValMatrix:
		mtx := vls.(model.Matrix)
		//fmt.Printf("Matrix %+v\n", mtx)
		tbl := matrixToLuaTable(L, mtx)
		return tbl, lua.LNil
	default:
		return lua.LNil, lua.LString(fmt.Sprintf("Not supported type %T", t))
	}
}

func (pc *PromClient) queryMetric(offset int, L *lua.LState) int {
	var vls model.Value
	var wrns v1.Warnings
	var err error
	qs, qr := buildQueryStatment(offset, L)
	//fmt.Printf("Query: %s\n", qs.String())
	//fmt.Printf("Start: %s End: %s Step: %s\n", qr.start, qr.end, qr.step)
	if qr != nil {
		var d time.Duration
		if qr.step != "" {
			d, err = time.ParseDuration(qr.step)
		}
		if err == nil {
			vls, wrns, err = pc.Api.QueryRange(context.Background(), qs.String(), v1.Range{
				Start: qr.start,
				End:   qr.end,
				Step:  d,
			})
		}
	} else {
		vls, wrns, err = pc.Api.Query(context.Background(), qs.String(), time.Time{})
	}
	if err != nil {
		fmt.Printf("Error query %s\n", err)
		L.Push(lua.LNil)
		L.Push(lua.LString(err.Error()))
		return 2
	}

	if len(wrns) > 0 {
		fmt.Printf("warning %+v\n", wrns)
	}
	vl1, vl2 := PromResultsToLua(L, vls)
	L.Push(vl1)
	L.Push(vl2)
	return 2
}

func queryReturnLua(L *lua.LState, client *PromClient, qs QueryStatement, qr queryRange) (lua.LValue, lua.LValue) {
	//fmt.Printf("Query: %s\n", qs.String())
	//fmt.Printf("Start: %s End: %s Step: %s\n", qr.start, qr.end, qr.step)
	var d time.Duration
	if qr.step != "" {
		// TODO: Figure out what to do with error here
		d, _ = time.ParseDuration(qr.step)
	}
	vls, wrns, err := client.Api.QueryRange(context.Background(), qs.String(), v1.Range{
		Start: qr.start,
		End:   qr.end,
		Step:  d,
	})
	if err != nil {
		fmt.Printf("Error query %s\n", err)
		return lua.LNil, lua.LString(err.Error())
	}

	if len(wrns) > 0 {
		fmt.Printf("warning %+v\n", wrns)
	}
	return PromResultsToLua(L, vls)
}

func matrixToLuaTable(L *lua.LState, mtx model.Matrix) *lua.LTable {
	tbl := L.NewTable()
	for _, v := range mtx {
		nt := L.NewTable()
		for i, s := range v.Values {
			nt.RawSetH(lua.LNumber(i), lua.LNumber(s.Value))
		}
		tbl.RawSetString(v.Metric.String(), nt)
	}
	return tbl

}
func (pc *PromClient) Metric(L *lua.LState) int {
	return pc.queryMetric(1, L)
}

const (
	PromLibName          = "prom"
	clientTypeName       = "promClient"
	AccumulationTypeName = "promAccumulation"
	timeSeriesTypeName   = "promTimeSeries"
	pollerTypeName       = "promPoller"
)

func newClient(L *lua.LState) int {
	host := "http://localhost:9090/"

	if L.ToString(1) != "" {
		host = L.CheckString(1)
	}
	c, err := NewPromClient(host)
	if err != nil {
		L.Push(lua.LNil)
		L.Push(lua.LString(err.Error()))
		return 2
	}
	ud := L.NewUserData()
	ud.Value = c
	L.SetMetatable(ud, L.GetTypeMetatable(clientTypeName))
	L.Push(ud)
	L.Push(lua.LNil)
	return 2
}

func formatTime(L *lua.LState) int {
	t := L.CheckNumber(1)
	f := L.CheckString(2)
	ts := time.Unix(0, int64(t)*int64(time.Millisecond))
	L.Push(lua.LString(ts.Format(f)))
	return 1
}

var promFuncs = map[string]lua.LGFunction{
	"new":         newClient,
	"format_time": formatTime,
}

func queryMetrics(L *lua.LState, client *PromClient, mtx []string, start, end time.Time, step string) (*lua.LTable, error) {
	qm := L.NewTable()
	rg := queryRange{
		start: start,
		end:   end,
		step:  step,
	}
	for _, m := range mtx {
		qs := QueryStatement{
			MetricName: m,
		}
		r, _ := queryReturnLua(L, client, qs, rg)
		qm.RawSetString(m, r)
	}
	return qm, nil
}

func changes(L *lua.LState) int {

	client := checkClient(L)
	if client == nil {
		L.Push(lua.LNil)
		L.Push(lua.LString("Failed to find client"))
		return 2
	}

	config := L.CheckTable(2)
	stream := config.RawGetString("stream").(*lua.LUserData)
	cs := stream.Value.(*cdelta.ChangeStream)

	match := cdelta.ChangeMatch{}
	mtcConf := config.RawGetString("match")
	if mtcConf != lua.LNil {

		name := config.RawGetString("name")
		if name != lua.LNil {
			match.Name = regexp.MustCompile(name.String())
		}
		repo := config.RawGetString("repo")
		if repo != lua.LNil {
			match.Repo = repo.String()
		}
		tp := config.RawGetString("type")
		if tp != lua.LNil {
			match.Type = tp.String()
		}
	}
	wind := config.RawGetString("window")
	if wind == lua.LNil {
		wind = lua.LString("5m")
	}
	ws := wind.(lua.LString)
	window, err := time.ParseDuration(ws.String())
	if err != nil {
		fmt.Printf("Failed to parse window %s\n", err)
		L.Push(lua.LNil)
		L.Push(lua.LString(fmt.Sprintf("Failed to parse window duration %s error: %s\n", ws.String(), err)))
		return 2
	}

	mtx := []string{}
	metrics := config.RawGetString("metrics").(*lua.LTable)
	metrics.ForEach(func(l1, l2 lua.LValue) {
		mtx = append(mtx, l2.String())
	})
	fmt.Printf("Metrics set are: %s\n", mtx)

	handler := config.RawGetString("handler").(*lua.LFunction)
	cs.Subscribe(cdelta.ChangeSubscription{

		Match: match,
		Handler: func(c cdelta.Change) {
			go func() {
				preStart := c.StartTime.Add(-window)
				preEnd := c.StartTime
				postStart := c.EndTime.Add(c.SettleDelay)
				postEnd := postStart.Add(window)

				//fmt.Printf("preStart %+v preEnd %+v postSTart %+v postEnd %+v now %+v\n", preStart, preEnd, postStart, postEnd, time.Now())
				waitTime := time.Until(postEnd)
				//fmt.Printf("Waiting %+v\n", waitTime)
				if waitTime > 0 {
					time.Sleep(waitTime)
				}
				ctx := context.Background()
				var changeErr lua.LValue
				data, err := QueryAndLoadState(L, ctx, client, mtx, NewWindow(preStart, preEnd), NewWindow(postStart, postEnd), ws.String())
				if err != nil {

					log.Printf("ERROR: %s\n", err)
					changeErr = lua.LString(err.Error())
				}
				paramData := L.NewTable()
				for m, d := range data {
					paramData.RawSetString(m, d)
				}
				chl := cdelta.ChangeToLuaTable(L, c)

				L.CallByParam(
					lua.P{
						Fn:   handler,
						NRet: 0,
					}, chl, paramData, changeErr)
			}()
		},
	})
	return 0
}

func checkClient(L *lua.LState) *PromClient {
	ud := L.CheckUserData(1)
	if v, ok := ud.Value.(*PromClient); ok {
		return v
	}
	L.ArgError(1, "client not found")
	return nil
}

func metric(L *lua.LState) int {
	client := checkClient(L)
	if client == nil {
		L.Push(lua.LNil)
		L.Push(lua.LString("Failed to connect to client"))
		return 2
	}
	return client.queryMetric(2, L)
}
func query(L *lua.LState) int {
	client := checkClient(L)
	if client == nil {
		L.Push(lua.LNil)
		L.Push(lua.LString("Failed to connect to client"))
		return 2
	}
	return client.queryMetric(2, L)
}

func clientSignal(L *lua.LState) int {
	client := checkClient(L)
	if client == nil {
		L.Push(lua.LNil)
		L.Push(lua.LString("Failed to find client"))
		return 2
	}
	var vls model.Value
	var wrns v1.Warnings
	var err error
	qs, qr := buildQueryStatment(2, L)
	//fmt.Printf("Query: %s\n", qs.String())
	//fmt.Printf("Start: %s End: %s Step: %s\n", qr.start, qr.end, qr.step)
	if qr != nil {
		var d time.Duration
		if qr.step != "" {
			d, err = time.ParseDuration(qr.step)
		}
		if err == nil {
			vls, wrns, err = client.Api.QueryRange(context.Background(), qs.String(), v1.Range{
				Start: qr.start,
				End:   qr.end,
				Step:  d,
			})
		}
	} else {
		vls, wrns, err = client.Api.Query(context.Background(), qs.String(), time.Time{})
	}
	if err != nil {
		fmt.Printf("Error query %s\n", err)
		L.Push(lua.LNil)
		L.Push(lua.LString(err.Error()))
		return 2
	}

	if len(wrns) > 0 {
		fmt.Printf("warning %+v\n", wrns)
	}
	//fmt.Printf("values %s\n", vls)
	//fmt.Printf("Values Type: %T\n", vls)
	t := vls.Type()
	switch t {
	case model.ValMatrix:
		mtx := vls.(model.Matrix)
		ud := L.NewUserData()
		ud.Value = &timeSeries{data: mtx}
		L.SetMetatable(ud, L.GetTypeMetatable(timeSeriesTypeName))
		L.Push(ud)
		L.Push(lua.LNil)
	default:
		L.Push(lua.LNil)
		L.Push(lua.LNil)
	}
	return 2
}

func ToTimeSeries(L *lua.LState, vls model.Value) (lua.LValue, lua.LValue) {
	t := vls.Type()
	switch t {
	case model.ValMatrix:
		mtx := vls.(model.Matrix)
		ud := L.NewUserData()
		ud.Value = &timeSeries{data: mtx}
		L.SetMetatable(ud, L.GetTypeMetatable(timeSeriesTypeName))
		return ud, lua.LNil
	default:
		return lua.LNil, lua.LNil
	}
}

var clientFuncs = map[string]lua.LGFunction{
	"metric":  metric,
	"query":   query,
	"signal":  clientSignal,
	"poll":    poll,
	"changes": changes,
}

func OpenProm(L *lua.LState) int {
	mod := L.RegisterModule(PromLibName, promFuncs).(*lua.LTable)

	ct := L.NewTypeMetatable(clientTypeName)
	L.SetGlobal(clientTypeName, ct)
	ct.RawSetString("__index", ct)
	L.SetFuncs(ct, clientFuncs)

	tt := L.NewTypeMetatable(timeSeriesTypeName)
	L.SetGlobal(timeSeriesTypeName, tt)
	tt.RawSetString("__index", tt)
	L.SetFuncs(tt, timeSeriesFuncs)

	at := L.NewTypeMetatable(AccumulationTypeName)
	L.SetGlobal(AccumulationTypeName, at)
	at.RawSetString("__index", at)
	L.SetFuncs(at, accumulationFuncs)

	pt := L.NewTypeMetatable(pollerTypeName)
	L.SetGlobal(pollerTypeName, pt)
	pt.RawSetString("__index", pt)
	L.SetFuncs(pt, pollerFuncs)

	L.Push(mod)
	return 1
}
