package prom

import (
	"testing"
	"time"
)

func TestHttpTotalQueryAST(t *testing.T) {

	expQuery := `http_requests_total`
	stmt := QueryStatement{
		MetricName: "http_requests_total",
	}

	if stmt.String() != expQuery {
		t.Fatalf("Expected '%s' got '%s'\n", expQuery, stmt.String())
	}
}

func TestHttpFilterQueryAST(t *testing.T) {
	expQuery := `http_requests_total{environment=~"staging|testing|development",method!="GET"}`
	stmt := QueryStatement{
		MetricName: "http_requests_total",
		Labels: []LabelMatch{
			{
				Label: "environment",
				Value: "staging|testing|development",
				Op:    Token{Type: REGEX, Literal: REGEX},
			},
			{
				Label: "method",
				Value: "GET",
				Op:    Token{Type: NOT_EQ, Literal: NOT_EQ},
			},
		},
	}

	if stmt.String() != expQuery {
		t.Fatalf("Expected '%s' got '%s'\n", expQuery, stmt.String())
	}
}

func TestRateAggStmtAST(t *testing.T) {

	expQuery := `rate by (host) (sndl_httphandler_sndl_http_response_seconds{method="GET"}[20m0s:5s])`

	stmt := AccStatement{

		Function: Token{Type: IDENT, Literal: "rate"},
		Query: QueryStatement{
			MetricName: "sndl_httphandler_sndl_http_response_seconds",
			Labels: []LabelMatch{
				{
					Label: "method",
					Value: "GET",
					Op:    Token{Type: EQ, Literal: EQ},
				},
			},
		},
		By:         []string{"host"},
		Past:       time.Duration(20 * time.Minute),
		Resolution: time.Duration(5 * time.Second),
	}

	if stmt.String() != expQuery {
		t.Fatalf("Expected '%s' got '%s'\n", expQuery, stmt.String())
	}
}
