package prom

import (
	"fmt"
	"strings"
	"time"
)

type Parser struct {
	lex *Lexer

	current Token
	peek    Token
}

func NewParser(l *Lexer) *Parser {
	p := &Parser{lex: l}

	p.nextToken()
	p.nextToken()

	return p
}

func (p *Parser) nextToken() {
	p.current = p.peek
	p.peek = p.lex.NextToken()
}

func (p *Parser) ParserStatement() Statement {

	var stmt Statement
	if IsAccumulationFunc(p.current.Literal) {
		stmt = p.parseAccStatement()
	} else {

		switch p.current.Type {
		case IDENT:
			stmt = p.parserQueryStatement()
		default:
			fmt.Printf("Type %s not supported literal '%s'\n", p.current.Type, p.current.Literal)
		}
	}
	return stmt
}

func (p *Parser) currentTokenIs(t TokenType) bool {
	return p.current.Type == t
}

func (p *Parser) peekTokenIs(t TokenType) bool {
	return p.peek.Type == t
}

func (p *Parser) parseLabels() []LabelMatch {
	lbls := []LabelMatch{}
	if p.peekTokenIs(RBRACE) || p.peekTokenIs(EOF) {
		return lbls
	}
	if p.currentTokenIs(LBRACE) {
		p.nextToken()
	}
	for !p.peekTokenIs(EOF) && !p.currentTokenIs(RBRACE) {
		if p.currentTokenIs(COMMA) {
			p.nextToken()
		}
		l := LabelMatch{
			Label: p.current.Literal,
		}
		p.nextToken()
		l.Op = p.current
		p.nextToken()
		if p.current.Type != TEXT {
			break
		}
		l.Value = p.current.Literal
		lbls = append(lbls, l)
		p.nextToken()
	}

	return lbls
}

func (p *Parser) parseQueryDuration() (time.Duration, error) {
	if p.currentTokenIs(RBRACKET) || p.peekTokenIs(RBRACKET) || p.currentTokenIs(EOF) || p.peekTokenIs(EOF) {
		return 0, nil
	}
	if p.peekTokenIs(LBRACKET) {
		p.nextToken()
	}
	if p.currentTokenIs(LBRACKET) {
		p.nextToken()
	}

	bits := ""
	for !p.currentTokenIs(RBRACKET) && !p.currentTokenIs(EOF) {
		bits = bits + p.current.Literal
		p.nextToken()
	}
	return time.ParseDuration(bits)

}
func (p *Parser) parserQueryStatement() *QueryStatement {

	q := &QueryStatement{MetricName: p.current.Literal}
	if !p.peekTokenIs(LBRACE) || p.peekTokenIs(EOF) {
		return q
	}

	p.nextToken()
	q.Labels = p.parseLabels()

	if p.currentTokenIs(LBRACKET) || p.peekTokenIs(LBRACKET) {
		d, err := p.parseQueryDuration()
		if err != nil {
			fmt.Printf("Failed to parse duration %s\n", err)
		}
		q.Duration = d
	}
	return q
}

func (p *Parser) parseBy() []string {
	lbls := []string{}
	if p.peekTokenIs(RBRACE) || p.peekTokenIs(EOF) {
		return lbls
	}
	if p.currentTokenIs(LBRACE) {
		p.nextToken()
	}
	for !p.peekTokenIs(EOF) || !p.currentTokenIs(RBRACE) {
		if p.currentTokenIs(COMMA) {
			p.nextToken()
		}
		if p.current.Type != TEXT {
			break
		}
		lbls = append(lbls, p.current.Literal)
		p.nextToken()
	}
	p.nextToken()
	return lbls
}

func (p *Parser) parsePastResolution() (time.Duration, time.Duration, error) {
	if p.currentTokenIs(RBRACKET) || p.peekTokenIs(RBRACKET) || p.currentTokenIs(EOF) || p.peekTokenIs(EOF) {
		return 0, 0, nil
	}
	if p.peekTokenIs(LBRACKET) {
		p.nextToken()
	}
	if p.currentTokenIs(LBRACKET) {
		p.nextToken()
	}

	bits := ""
	for !p.currentTokenIs(RBRACKET) && !p.currentTokenIs(EOF) {
		bits = bits + p.current.Literal
		p.nextToken()
	}
	sp := strings.Split(bits, ":")

	past, err := time.ParseDuration(sp[0])
	if err != nil {
		return 0, 0, err
	}
	var r time.Duration
	if len(sp) > 1 {
		r, err = time.ParseDuration(sp[1])
		if err != nil {
			return 0, 0, err
		}
	}
	return past, r, nil

}
func (p *Parser) parseAccStatement() *AccStatement {
	lbls := []string{}
	fnc := p.current
	p.nextToken()
	if p.currentTokenIs(BY) {
		p.nextToken()
		lbls = p.parseBy()
		p.nextToken()
		p.nextToken()
	}
	if p.currentTokenIs(LPAREN) {
		p.nextToken()
	}
	qs := p.parserQueryStatement()
	p.nextToken()
	pst, res, err := p.parsePastResolution()
	if err != nil {
		fmt.Printf("Error parsing time: %s\n", err)
	}
	return &AccStatement{Function: fnc, Query: *qs, By: lbls, Past: pst, Resolution: res}
}
