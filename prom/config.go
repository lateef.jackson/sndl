package prom

import (
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"

	"gopkg.in/yaml.v3"
)

type PromConfigType string

const (
	WindowConfig PromConfigType = "window"
	ChangeConfig PromConfigType = "change"
)

type PromCfg struct {
	Type  string `yaml:"type"`
	Match struct {
		Repo   string            `yaml:"repo"`
		Type   string            `yaml:"type"`
		Labels map[string]string `yaml:"labels"`
	}
	Window  string   `yaml:"window"`
	Step    string   `yaml:"step"`
	Metrics []string `yaml:"metrics"`
	Expect  []string `yaml:"expect"`
}

type PromConfigs struct {
	Configs map[string]*PromCfg
}

func LoadDirConfig(pc *PromConfigs, path string) error {
	return filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() && strings.Contains(path, ".yml") {
			c, err := loadPromChangeCfgFromFile(path)
			if err != nil {
				// XXX: The idea is if there are other yaml files it will just log the error and move on. Should it hard fail? or better have a way of filtering in the path so it always matches the correct yaml files?
				log.Printf("Failed to parse yaml file %s with error '%s'\n", path, err)
				return nil
			}
			pc.Configs[path] = c
		}
		return nil
	})
}

func LoadFromPaths(path string) (*PromConfigs, error) {
	pc := PromConfigs{Configs: map[string]*PromCfg{}}
	for _, p := range strings.Split(path, ",") {
		fi, err := os.Stat(p)
		if err != nil {
			return nil, err
		}
		if fi.IsDir() {
			err := LoadDirConfig(&pc, p)
			if err != nil {
				return nil, err
			}
		} else {
			c, err := loadPromChangeCfgFromFile(p)
			if err != nil {
				return nil, err
			}
			pc.Configs[p] = c
		}
	}
	return &pc, nil
}

func loadPromChangeCfgFromFile(path string) (*PromCfg, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	bits, err := io.ReadAll(f)
	if err != nil {
		return nil, err
	}
	return parsePromChangeCfg(bits)
}

func parsePromChangeCfg(bits []byte) (*PromCfg, error) {
	pcc := PromCfg{}
	err := yaml.Unmarshal(bits, &pcc)
	return &pcc, err
}
