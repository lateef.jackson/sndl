package prom

type Lexer struct {
	input      []byte
	offset     int // current input position
	nextOffset int // next char position
	ch         byte
}

func Lex(input []byte) *Lexer {
	l := &Lexer{input: input}
	l.readChar()
	return l
}

func (l *Lexer) readChar() {
	if l.nextOffset >= len(l.input) {
		l.ch = 0
	} else {
		l.ch = l.input[l.nextOffset]
	}
	l.offset = l.nextOffset
	l.nextOffset += 1
}

func (l *Lexer) NextToken() Token {
	var tok Token

	l.skipWhitespace()

	switch l.ch {
	case '=':
		if l.peekChar() == '=' {
			ch := l.ch
			l.readChar()
			literal := string(ch) + string(l.ch)
			tok = Token{Type: DOUBLE_EQ, Literal: literal}
		} else if l.peekChar() == '~' {
			ch := l.ch
			l.readChar()
			literal := string(ch) + string(l.ch)
			tok = Token{Type: REGEX, Literal: literal}
		} else {
			tok = Token{Type: EQ, Literal: string(l.ch)}
		}
	case ';':
		tok = Token{Type: SEMICOLON, Literal: string(l.ch)}
	case ':':
		tok = Token{Type: COLON, Literal: string(l.ch)}
	case '{':
		tok = Token{Type: LBRACE, Literal: string(l.ch)}
	case '}':
		tok = Token{Type: RBRACE, Literal: string(l.ch)}
	case '[':
		tok = Token{Type: LBRACKET, Literal: string(l.ch)}
	case ']':
		tok = Token{Type: RBRACKET, Literal: string(l.ch)}
	case '(':
		tok = Token{Type: LPAREN, Literal: string(l.ch)}
	case ')':
		tok = Token{Type: RPAREN, Literal: string(l.ch)}
	case ',':
		tok = Token{Type: COMMA, Literal: string(l.ch)}
	case '+':
		tok = Token{Type: PLUS, Literal: string(l.ch)}
	case '-':
		tok = Token{Type: MINUS, Literal: string(l.ch)}
	case '~': //  Regex match so yeah ...
		if l.peekChar() == '=' {
			ch := l.ch
			l.readChar()
			literal := string(ch) + string(l.ch)
			tok = Token{Type: REGEX, Literal: literal}
		} else {
			tok = Token{Type: TILDE, Literal: string(l.ch)}
		}
	case '@':
		tok = Token{Type: AT, Literal: string(l.ch)}
	case '!':
		if l.peekChar() == '=' {
			ch := l.ch
			l.readChar()
			literal := string(ch) + string(l.ch)
			tok = Token{Type: NOT_EQ, Literal: literal}
		} else if l.peekChar() == '~' {
			ch := l.ch
			l.readChar()
			literal := string(ch) + string(l.ch)
			tok = Token{Type: NOT_REGEX, Literal: literal}

		} else {
			tok = Token{Type: BANG, Literal: string(l.ch)}
		}
	case '/':
		tok = Token{Type: SLASH, Literal: string(l.ch)}
	case '*':
		tok = Token{Type: ASTERISK, Literal: string(l.ch)}
	case '>':
		tok = Token{Type: LT, Literal: string(l.ch)}
	case '<':
		tok = Token{Type: GT, Literal: string(l.ch)}
	case '"':
		tok = Token{Type: TEXT, Literal: l.readString()}
	case 0:
		tok = Token{Type: EOF, Literal: ""}
	default:
		if isAlpha(l.ch) {
			lit := l.readIdentifier()
			return Token{Type: LookupIdent(lit), Literal: lit}
		} else if isNumeric(l.ch) {
			return Token{Type: INT, Literal: l.readNumber()}
		} else {
			return Token{Type: ILLEGAL, Literal: string(l.ch)}
		}
	}
	l.readChar()
	return tok
}

func (l *Lexer) readIdentifier() string {
	start := l.offset
	for isAlpha(l.ch) {
		l.readChar()
	}
	return string(l.input[start:l.offset])
}

func (l *Lexer) readNumber() string {
	start := l.offset
	for isNumeric(l.ch) {
		l.readChar()
	}
	return string(l.input[start:l.offset])
}

func (l *Lexer) peekChar() byte {
	if l.offset >= len(l.input) {
		return 0
	} else {
		return l.input[l.nextOffset]
	}
}

func isAlpha(ch byte) bool {
	return 'a' <= ch && ch <= 'z' || 'A' <= ch && ch <= 'Z' || ch == '_'
}

func isNumeric(ch byte) bool {
	return '0' <= ch && ch <= '9'
}

func isAlphaNumeric(ch byte) bool {
	return isAlpha(ch) || isNumeric(ch)
}

func (l *Lexer) skipWhitespace() {
	for IsWhiteSpaceRune(l.ch) {
		l.readChar()
	}
}

func (l *Lexer) readString() string {
	start := l.offset + 1
	for {
		l.readChar()
		if l.ch == DOUBLE_QUOTE || l.ch == 0 {
			break
		}
	}
	return string(l.input[start:l.offset])
}
