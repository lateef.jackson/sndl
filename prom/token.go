package prom

import (
	"strings"
)

const (
	ILLEGAL = "ILLEGAL"
	EOF     = "EOF"
	// Identifies + literals
	IDENT = "IDENT" // add, foobar, x, y, ...
	INT   = "INT"
	TEXT  = "TEXT"

	// Operators
	PLUS     = "+"
	MINUS    = "-"
	BANG     = "!"
	ASTERISK = "*"
	SLASH    = "/"
	TILDE    = "~"
	AT       = "@"

	EQ        = "="
	LT        = "<"
	GT        = ">"
	DOUBLE_EQ = "=="
	NOT_EQ    = "!="
	REGEX     = "=~"
	NOT_REGEX = "!~"

	// Comments
	COMMENT = "//"

	// Delimiters
	COMMA     = ","
	SEMICOLON = ";"
	COLON     = ":"

	LPAREN   = "("
	RPAREN   = ")"
	LBRACE   = "{"
	RBRACE   = "}"
	LBRACKET = "["
	RBRACKET = "]"

	// White Space
	SPACE       = ' '
	TAB         = '\t'
	NEW_LINE    = '\n'
	RETURN_LINE = '\r'

	// Quotes
	DOUBLE_QUOTE = '"'
	SINGLE_QUOTE = '\''

	// Ops
	AND    = "and"
	OR     = "or"
	UNLESS = "unless"
	ATAN2  = "atan2"

	// Accumulators
	SUM          = "sum"
	AVG          = "avg"
	RATE         = "rate"
	COUNT        = "count"
	MIN          = "min"
	MAX          = "max"
	GROUP        = "group"
	STDDEV       = "stddev"
	STDVAR       = "stdvar"
	BOTTOMK      = "bottomk"
	COUNT_VALUES = "count_values"
	QUANTILE     = "quantile"

	// Keywords
	OFFSET      = "offset"
	BY          = "by"
	WITHOUT     = "without"
	ON          = "on"
	IGNORING    = "ignoring"
	GROUP_LEFT  = "group_left"
	GROUP_RIGHT = "group_right"
	BOOL        = "bool"

	// Preproc
	START = "start"
	END   = "end"
)

var (
	EqToken       = Token{EQ, EQ}
	RegexToken    = Token{REGEX, REGEX}
	NotEqToken    = Token{NOT_EQ, NOT_EQ}
	NotRegexToken = Token{NOT_REGEX, NOT_REGEX}
)

var WhiteSpace = []byte{' ', '\t', '\n', '\r'}

func IsWhiteSpaceRune(c byte) bool {

	for _, r := range WhiteSpace {
		if r == c {
			return true
		}
	}
	return false
}

type TokenType string

type Token struct {
	Type    TokenType
	Literal string
}

var keywords = map[string]TokenType{
	AND:    AND,
	OR:     OR,
	UNLESS: UNLESS,
	ATAN2:  ATAN2,

	SUM:          SUM,
	AVG:          AVG,
	RATE:         RATE,
	COUNT:        COUNT,
	MIN:          MIN,
	MAX:          MAX,
	GROUP:        GROUP,
	STDDEV:       STDDEV,
	STDVAR:       STDVAR,
	BOTTOMK:      BOTTOMK,
	COUNT_VALUES: COUNT_VALUES,
	QUANTILE:     QUANTILE,

	OFFSET:      OFFSET,
	BY:          BY,
	WITHOUT:     WITHOUT,
	ON:          ON,
	IGNORING:    IGNORING,
	GROUP_LEFT:  GROUP_LEFT,
	GROUP_RIGHT: GROUP_RIGHT,
	BOOL:        BOOL,

	START: START,
	END:   END,
}

func LookupIdent(ident string) TokenType {
	k := strings.ToLower(ident)
	if tok, ok := keywords[k]; ok {
		return tok
	}
	return IDENT
}

type AccumulativeFunction func()

// TODO: Implement cumulative functions
var AccumulativeFuncs = map[string]AccumulativeFunction{
	RATE:         func() {},
	SUM:          func() {},
	AVG:          func() {},
	COUNT:        func() {},
	MIN:          func() {},
	MAX:          func() {},
	GROUP:        func() {},
	STDDEV:       func() {},
	STDVAR:       func() {},
	BOTTOMK:      func() {},
	COUNT_VALUES: func() {},
	QUANTILE:     func() {},
}

func IsAccumulationFunc(txt string) bool {
	_, ok := AccumulativeFuncs[txt]
	return ok
}

var (
	OffsetToken     = Token{Type: OFFSET, Literal: OFFSET}
	ByToken         = Token{Type: BY, Literal: BY}
	WithoutToken    = Token{Type: WITHOUT, Literal: WITHOUT}
	OnToken         = Token{Type: ON, Literal: ON}
	IgnoringToken   = Token{Type: IGNORING, Literal: IGNORING}
	GroupLeftToken  = Token{Type: GROUP_LEFT, Literal: GROUP_LEFT}
	GroupRightToken = Token{Type: GROUP_RIGHT, Literal: GROUP_RIGHT}
	BoolToken       = Token{Type: BOOL, Literal: BOOL}
	StartToken      = Token{Type: START, Literal: START}
	EndToken        = Token{Type: END, Literal: END}
)
