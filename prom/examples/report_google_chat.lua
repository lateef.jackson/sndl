local os = require("os")
local stache = require("stache")
local chat = require("chat")
local prom = require("prom")

local wh = os.getenv("GCHAT_WEB_HOOK")
if wh == nil then
	print("ERROR no environment variable GCHAT_WEB_HOOK set!!!")
end
local client, err = prom:new()
if err ~= nil then
	print("Error creating prom client: " .. err)
end

local gchat = chat.google(wh)
local cpu_poll = client:poll("node_cpu_seconds_total", {
	labels = { mode = "user" },
	duration = "12h",
	interval = "12h",
	handler = function(sig)
		local delta = sig:delta()
		local txt, rerr = stache.render_file("prom/examples/report_google_chat.tmpl", {
			time = os.date("%c"),
			cpu_median = string.format("%.4f", delta:median()),
			cpu_average = string.format("%.4f", delta:average()),
		})
		if rerr ~= nil then
			print(string.format("ERROR rendering file %s", rerr))
			return
		end
		gchat:send({ text = txt })
	end,
})

cpu_poll:start()

-- Configuration table to track state and threasholds
local config = {
	threshold_seconds = 20, -- Seconds
	reminder_threashold = 60 * 60, -- Send reminders to the thread every hour
	last_signal = nil,
	last_reported = nil,
	voliated = nil,
	notified = nil,
	thread_key = nil,
}

-- Function to calculate late data
local late_data = function(last)
	local seconds = os.time() - last
	return seconds > config.threshold_seconds, seconds
end

-- Notification Logic
local notify = function(last, delay, recovered)
	local now = os.time()
	local delay_fmt = string.format("%0.4f seconds", delay)
	if last == 0 then
		delay_fmt = "10 minutes"
	end
	local msg = string.format(
		"%s Sndl threahold is %0.4f web hasn't reported in over %s",
		os.date("%Y-%m-%d %H:%M:%S"),
		config.threshold_seconds,
		delay_fmt
	)
	if config.thread_key == nil then
		config.thread_key = string.format("%d-%0.4f", now, delay)
	end
	local payload = {
		text = msg,
		thread = { threadKey = config.thread_key },
	}
	if recovered then
		-- Change the message a bit if recovered from violation
		payload.text = string.format(
			"🎉 %s Sndl threahold is %0.4f web recovered in over %s ",
			os.date("%Y-%m-%d %H:%M:%S"),
			config.threshold_seconds,
			delay_fmt
		)
		gchat:send(payload)
	elseif config.notified == nil then
		gchat:send(payload)
	elseif now - config.notified > config.reminder_threashold then
		-- Change the message to reminder
		payload.text = "Reminder: " .. payload.text
		gchat:send(payload)
	else
		return -- No op no need to do anything else
	end
	config.notified = now
end
-- Poll on a frequent basis
local resp_time_poll = client:poll("sndl_httphandler_sndl_http_response_seconds", {
	labels = { service = "sndl" },
	duration = "10m",
	interval = string.format("%ds", config.threshold_seconds / 4), -- Check fourt times every threshold
	run_on_start = true,
	handler = function(sig)
		config.last_signal = os.time()
		local _, last = sig:basic():time_range()
		local violating, delay = late_data(last)
		print(
			string.format(
				"Last reported is %0.4f with delay %0.4f sample size %d",
				last,
				delay,
				sig:basic():sample_size()
			)
		)
		if violating then
			notify(last, delay, false)
		elseif config.violating and not violating then
			notify(last, delay, true)
		else
			config.violating = false
			config.notified = nil
			config.thread_key = nil
		end
		config.violating = violating
		config.last_reported = last
	end,
})
resp_time_poll:start()
-- Wait for ever
resp_time_poll:wait()
