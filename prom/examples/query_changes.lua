--[[
curl -X POST 'http://localhost:7070/change' -H 'Content-Type: application/json' -d '{"name":"foo", "type": "deploy", "repo": "repo","start_time": "2023-08-04T13:30:58Z", "end_time":"2023-08-04T13:38:07Z", "settle_delay": "15m",  "labels": {"foo":"bar", "service":"website"}}'
--]]

local math = require("math")
local prom = require("prom")
local delta = require("delta")
local cpu_metric_name = "node_cpu_seconds_total"
local load1_metric_name = "node_load1"
local stream = delta.stream()

local client, err = prom:new()
if err ~= nil then
	print("Error creating prom client: " .. err)
end

client:changes({
	stream = stream, -- Change stream
	match = { -- Stream filter match criteria
		type = "deploy",
	},
	metrics = { -- Metrics to query
		cpu_metric_name,
		load1_metric_name,
	},
	window = "10m", -- Data sample window
	handler = function(change, data, cerr) -- Function handler
		if err ~= nil then
			print(string.format("Handle got error from sndl server %s", cerr))
			return
		end
		if data.node_load1.diff == nil then
			print("No data for node_load1")
			return
		end
		if data.node_cpu_seconds_total.diff == nil then
			print("No data for node_cpu_seconds_total ")
			return
		end
		local cpu_change = data.node_cpu_seconds_total.diff:sum()
		local load_change = data.node_load1.diff:median()

		print(string.format("Load has change to %0.4f and cpu_change is changed %0.4f", load_change, cpu_change))
	end,
})
-- Listen for http POST changes
stream:listen(":7071", "/change")
