local prom = require("prom")

local client, err = prom:new()
if err ~= nil then
	print("Error creating prom client: " .. err)
end

local sig, merr = client:signal("node_cpu_seconds_total", { labels = { mode = "idle" }, duration = "10m" })
if merr ~= nil then
	print("Error doing CPU queries: " .. err)
end
local basic = sig:basic()
local start_t, end_t = basic:time_range()
print("=============================== idle =========================")
print(
	"Start time: "
		.. prom.format_time(start_t, "Mon, 02 Jan 2006 15:04:05 MST")
		.. " End time: "
		.. prom.format_time(end_t, "Mon, 02 Jan 2006 15:04:05 MST")
)
print("")
print("Median is " .. string.format("%.4f", basic:median()))
print("Average is " .. string.format("%.4f", basic:average()))
print("Total is " .. string.format("%.4f", basic:total()))
print("SampleSize is " .. string.format("%.4f", basic:sample_size()))
print("Recomputed Average is " .. string.format("%.4f", (basic:total() / basic:sample_size())))

print("=============================== User =========================")
print("")
sig, merr = client:signal("node_cpu_seconds_total", { labels = { mode = "user" }, duration = "10m" })
if merr ~= nil then
	print("Error doing CPU queries: " .. err)
end
basic = sig:basic()
print("Median is " .. string.format("%.4f", basic:median()))
print("Average is " .. string.format("%.4f", basic:average()))
print("Total is " .. string.format("%.4f", basic:total()))
print("SampleSize is " .. string.format("%.4f", basic:sample_size()))
print("Recomputed Average is " .. string.format("%.4f", (basic:total() / basic:sample_size())))

print("=============================== system =========================")
print("")
sig, merr = client:signal("node_cpu_seconds_total", { labels = { mode = "system" }, duration = "10m" })
if merr ~= nil then
	print("Error doing CPU queries: " .. err)
end
basic = sig:basic()
print("Median is " .. string.format("%.4f", basic:median()))
print("Average is " .. string.format("%.4f", basic:average()))
print("Total is " .. string.format("%.4f", basic:total()))
print("SampleSize is " .. string.format("%.4f", basic:sample_size()))
print("Recomputed Average is " .. string.format("%.4f", (basic:total() / basic:sample_size())))

print("=============================== DELTA =========================")
sig, merr = client:signal("node_cpu_seconds_total", { labels = { mode = "idle" }, duration = "2m" })
if merr ~= nil then
	print("Error doing CPU queries: " .. err)
end
start_t, end_t = sig:basic():time_range()
print(
	"Start time: "
		.. prom.format_time(start_t, "Mon, 02 Jan 2006 15:04:05 MST")
		.. " End time: "
		.. prom.format_time(end_t, "Mon, 02 Jan 2006 15:04:05 MST")
)
print("")
local min_range, max_range = sig:delta():time_range()
print("Metric scrapping range from: " .. min_range .. " End time: " .. max_range)
print("")
print("=============================== idle =========================")
local delta = sig:delta()
local function print_data(data)
	for m, vls in pairs(data) do
		print("metric: " .. m)
		local t = {}
		for _, v in pairs(vls) do
			table.insert(t, v.value)
		end
		print(table.concat(t, ","))
	end
end
print("Min is " .. string.format("%.4f", delta:min()))
print("Max is " .. string.format("%.4f", delta:max()))
print("Median is " .. string.format("%.4f", delta:median()))
print("Average is " .. string.format("%.4f", delta:average()))
print("Total is " .. string.format("%.4f", delta:total()))
print("SampleSize is " .. string.format("%.4f", delta:sample_size()))
print("Recomputed Average is " .. string.format("%.4f", (delta:total() / delta:sample_size())))
print_data(delta:data())
print("=============================== User =========================")
print("")
sig, merr = client:signal("node_cpu_seconds_total", { labels = { mode = "user" }, duration = "2m" })
if merr ~= nil then
	print("Error doing CPU queries: " .. err)
end
delta = sig:delta()
print("Min is " .. string.format("%.4f", delta:min()))
print("Max is " .. string.format("%.4f", delta:max()))
print("Median is " .. string.format("%.4f", delta:median()))
print("Average is " .. string.format("%.4f", delta:average()))
print_data(delta:data())
print("Total is " .. string.format("%.4f", delta:total()))
print("SampleSize is " .. string.format("%.4f", delta:sample_size()))
print("Recomputed Average is " .. string.format("%.4f", (delta:total() / delta:sample_size())))
print("=============================== system =========================")
print("")
sig, merr = client:signal("node_cpu_seconds_total", { labels = { mode = "system" }, duration = "2m" })
if merr ~= nil then
	print("Error doing CPU queries: " .. err)
end
delta = sig:delta()
print("Min is " .. string.format("%.4f", delta:min()))
print("Max is " .. string.format("%.4f", delta:max()))
print("Median is " .. string.format("%.4f", delta:median()))
print("Average is " .. string.format("%.4f", delta:average()))
print("Total is " .. string.format("%.4f", delta:total()))
print("SampleSize is " .. string.format("%.4f", delta:sample_size()))
print("Recomputed Average is " .. string.format("%.4f", (delta:total() / delta:sample_size())))
