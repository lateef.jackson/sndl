local prom = require("prom")

local client, err = prom:new()
if err ~= nil then
	print("Error creating prom client: " .. err)
end

local cpu_delta = nil
local cpu_poll = client:poll("node_cpu_seconds_total", {
	labels = { mode = "user" },
	duration = "2m",
	interval = "10s",
	handler = function(sig)
		cpu_delta = sig:delta()
	end,
})
cpu_poll:start()
local http = require("http")
local stache = require("stache")
http.publish_metrics("/metrics")
http.match("/node_cpu_seconds_total/user", function(resp, req)
	if cpu_delta ~= nil then
		local sig, merr = client:signal("node_network_transmit_bytes_total", { duration = "2m" })

		local net_trans_delta = sig:delta()
		local html, rerr = stache.render_file("prom/examples/bytes_handler.html", {
			min = cpu_delta:min(),
			max = string.format("%.4f", cpu_delta:max()),
			median = string.format("%.4f", cpu_delta:median()),
			average = cpu_delta:average(),
			sample_size = cpu_delta:sample_size(),
			net_trans_min = net_trans_delta:min(),
			net_trans_max = net_trans_delta:max(),
			net_trans_median = string.format("%.4f", net_trans_delta:median()),
			net_trans_average = string.format("%.4f", net_trans_delta:average()),
			net_trans_sample_size = net_trans_delta:sample_size(),
		})
		if merr ~= nil then
			print("ERROR rendering template: " .. rerr)
		else
			resp:write(html)
		end
	end
end)
http.listen(":8012")
