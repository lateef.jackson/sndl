local light = require("light")
-- Poll traffic light metrics and update
local load_lights = {}
local load_eval = function(self)
	local last = self.buffer[#self.buffer]
	if last > 0.5 then
		return light.RED
	elseif last > 0.25 and last <= 0.5 then
		return light.YELLOW
	end
	return light.GREEN
end

local prom = require("prom")
local client, err = prom:new()
if err ~= nil then
	print("Error creating prom client: " .. err)
end

local load_poll = client:poll("node_load1", {
	duration = "2m",
	interval = "10s",
	handler = function(sig)
		for n, d in pairs(sig:basic():data()) do
			local l = load_lights[n]
			if l == nil then
				l = light:create(n, 5, load_eval)
				load_lights[n] = l
			end
			l:add(d[#d].value)
		end
	end,
})
load_poll:start()
local http = require("http")
local stache = require("stache")
http.publish_metrics("/metrics")

http.match("/", function(resp, _)
	local lts = {}
	for n, l in pairs(load_lights) do
		table.insert(lts, #lts + 1, {
			name = n,
			current = l.buffer[#l.buffer],
			data = l.buffer,
			color = l:eval(),
		})
	end
	local html, rerr = stache.render_file("prom/examples/trafficlight/index.html", {
		color = "red",
		lights = lts,
	})
	if rerr ~= nil then
		resp:write("Error rendering page " .. rerr)
	else
		resp:write(html)
	end
end)

http.listen(":8211")
