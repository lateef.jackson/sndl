Light = {}
Light.__index = Light

Light.RED = "red"
Light.YELLOW = "yellow"
Light.GREEN = "green"

function Light:create(name, size, evaluator)
	local l = {}
	setmetatable(l, Light)
	l.name = name
	l.size = size
	l.evaluator = evaluator
	l.buffer = {}
	return l
end

function Light:add(value)
	if #self.buffer >= self.size then
		table.remove(self.buffer, 1)
	end
	table.insert(self.buffer, #self.buffer + 1, value)
end

function Light:eval()
	if self.evaluator ~= nil then
		return self.evaluator(self)
	end
	print("Failed evaluator is nil")
	return nil
end

return Light
