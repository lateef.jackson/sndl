local prom = require("prom")

local client, err = prom:new()
if err ~= nil then
	print("Error creating prom client: " .. err)
end

local sig, merr = client:signal("sndl_httphandler_sndl_http_response_seconds", {
	labels = { cluster = "homelab" },
	duration = "2m",
})
if merr ~= nil then
	print("Yikes error getting signa: " .. merr)
else
	print("Median node_network_transmit_bytes_total is: " .. string.format("%.4f", sig:delta():max()))
end
