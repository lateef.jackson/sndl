local prom = require("prom")

local client, err = prom:new()
if err ~= nil then
	print("Error creating prom client: " .. err)
end

local sig, merr = client:signal("node_network_transmit_bytes_total", { duration = "10m" })
if merr ~= nil then
	print("Error doing CPU queries: " .. err)
end
local delta = sig:delta()
print("Min is " .. string.format("%.4f", delta:min() / 1024))
print("Max is " .. string.format("%.4f", delta:max() / 1024))
print("Median is " .. string.format("%.4f", delta:median() / 1024))
print("Average is " .. string.format("%.4f", delta:average() / 1024))
print("Total is " .. string.format("%.4f", delta:total() / 1024))
print("SampleSize is " .. string.format("%d", delta:sample_size()))
print("Recomputed Average is " .. string.format("%.4f", (delta:total() / delta:sample_size())))
