package prom

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	v1 "github.com/prometheus/client_golang/api/prometheus/v1"
	"github.com/prometheus/common/model"
	lua "github.com/yuin/gopher-lua"
)

var pollers []*signalPoller

// TODO: Lua state can not run on multiple threads remove this lock for better concurrency
var gilLock sync.Mutex

func init() {

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		for _, sp := range pollers {
			if sp.running {
				sp.stop()
			}
		}
		os.Exit(1)
	}()
}

type signalPoller struct {
	L          *lua.LState
	client     *PromClient
	query      string
	handler    *lua.LFunction
	timeSeries *timeSeries
	duration   time.Duration
	exit       chan bool
	wg         sync.WaitGroup
	runAtStart bool
	running    bool
}

func newSignalPoller(mtx model.Matrix, L *lua.LState, client *PromClient, query string, dur time.Duration, handler *lua.LFunction, runAtStart bool) *signalPoller {

	sp := &signalPoller{
		timeSeries: &timeSeries{data: mtx},
		L:          L,
		client:     client,
		query:      query,
		exit:       make(chan bool),
		duration:   dur,
		handler:    handler,
		runAtStart: runAtStart,
	}
	pollers = append(pollers, sp)
	return sp
}

func (sp *signalPoller) run() {
	vls, wrns, err := sp.client.Api.Query(context.Background(), sp.query, time.Time{})
	if err != nil {
		fmt.Printf("Error query %s\n", err)
		sp.L.Push(lua.LNil)
		sp.L.Push(lua.LString(err.Error()))
		return
	}
	if len(wrns) > 0 {
		fmt.Printf("warning %+v\n", wrns)
	}
	t := vls.Type()
	switch t {
	case model.ValMatrix:
		mtx := vls.(model.Matrix)
		ud := sp.L.NewUserData()
		ud.Value = &timeSeries{data: mtx}
		sp.L.SetMetatable(ud, sp.L.GetTypeMetatable(timeSeriesTypeName))
		// TODO: Lua state can not run on multiple threads
		gilLock.Lock()
		defer gilLock.Unlock()
		sp.L.CallByParam(lua.P{
			Fn:   sp.handler,
			NRet: 0,
		}, ud)
	default:
		fmt.Printf("ERROR: type not support for query %T", t)
	}
}

func (sp *signalPoller) start() bool {
	if sp.running {
		return false
	}
	sp.running = true
	sp.wg.Add(1)
	if sp.runAtStart {
		sp.run()
	}
	go func() {
		for {
			select {
			case <-sp.exit:
				return
			case <-time.After(sp.duration):
				sp.run()
			}
		}
	}()

	return false
}

func (sp *signalPoller) wait() {
	sp.wg.Wait()
}

func (sp *signalPoller) stop() bool {
	sp.exit <- true
	sp.wg.Done()
	sp.running = false
	return true
}

func checkPoller(L *lua.LState) *signalPoller {
	ud := L.CheckUserData(1)
	if v, ok := ud.Value.(*signalPoller); ok {
		return v
	}
	L.ArgError(1, "time series not found")
	return nil
}

func startPoller(L *lua.LState) int {
	poller := checkPoller(L)
	if poller == nil {
		L.Push(lua.LNil)
		L.Push(lua.LString("Failed to find to poller"))
		return 2
	}
	L.Push(lua.LBool(poller.start()))
	L.Push(lua.LNil)
	return 1
}

func waitPoller(L *lua.LState) int {
	poller := checkPoller(L)
	if poller == nil {
		L.Push(lua.LString("Failed to get poller"))
		return 1
	}
	poller.wait()
	return 0
}
func stopPoller(L *lua.LState) int {
	poller := checkPoller(L)
	if poller == nil {
		L.Push(lua.LNil)
		L.Push(lua.LString("Failed to get poller"))
		return 1
	}
	L.Push(lua.LBool(poller.stop()))
	L.Push(lua.LNil)
	return 1
}

var pollerFuncs = map[string]lua.LGFunction{
	"start": startPoller,
	"stop":  stopPoller,
	"wait":  waitPoller,
}

func poll(L *lua.LState) int {
	client := checkClient(L)
	if client == nil {
		L.Push(lua.LNil)
		L.Push(lua.LString("Failed to connect to client"))
		return 2
	}
	var vls model.Value
	var wrns v1.Warnings
	var err error
	qs, qr := buildQueryStatment(2, L)
	//fmt.Printf("Query: %s\n", qs.String())
	if qr != nil {
		var d time.Duration
		if qr.step != "" {
			d, err = time.ParseDuration(qr.step)
		}
		if err == nil {
			vls, wrns, err = client.Api.QueryRange(context.Background(), qs.String(), v1.Range{
				Start: qr.start,
				End:   qr.end,
				Step:  d,
			})
		}
	} else {
		vls, wrns, err = client.Api.Query(context.Background(), qs.String(), time.Time{})
	}
	if err != nil {
		fmt.Printf("Error query %s\n", err)
		L.Push(lua.LNil)
		L.Push(lua.LString(err.Error()))
		return 2
	}
	if len(wrns) > 0 {
		fmt.Printf("warning %+v\n", wrns)
	}
	config := L.CheckTable(3)
	tm := config.RawGetString("interval").String()
	dir, err := time.ParseDuration(tm)
	if err != nil {
		L.Push(lua.LNil)
		L.Push(lua.LString(fmt.Sprintf("Parse timer error: '%s'", err)))
		return 2
	}
	runAtStart := false
	ras := config.RawGetString("run_on_start")
	if ras != lua.LNil {
		if ras.Type() == lua.LTBool {
			r, ok := ras.(lua.LBool)
			if ok {
				runAtStart = bool(r)
			}

		}
	}

	handler := config.RawGetString("handler").(*lua.LFunction)
	t := vls.Type()
	switch t {
	case model.ValMatrix:
		mtx := vls.(model.Matrix)
		ud := L.NewUserData()
		ud.Value = newSignalPoller(mtx, L, client, qs.String(), dir, handler, runAtStart)
		L.SetMetatable(ud, L.GetTypeMetatable(pollerTypeName))
		L.Push(ud)
		L.Push(lua.LNil)
	default:
		L.Push(lua.LNil)
		L.Push(lua.LNil)
	}
	return 2
}
