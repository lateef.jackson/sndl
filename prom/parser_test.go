package prom

import (
	"testing"
	"time"
)

func TestQueryStatement(t *testing.T) {
	input := []byte(`http_requests_total`)
	l := Lex(input)
	p := NewParser(l)

	stmt := p.ParserStatement()
	if stmt == nil {
		t.Fatal("ParseStatement() returned nil")
	}

	qstmt, ok := stmt.(*QueryStatement)
	if !ok {
		t.Errorf("s not *QueryStatement. got=%T", qstmt)
	}

	if qstmt.String() != string(input) {
		t.Errorf("Expected %s however got %s\n", string(input), qstmt.String())
	}

	lblInput := []byte(`http_requests_total{job="prometheus",group="canary"}`)
	l = Lex(lblInput)
	p = NewParser(l)

	stmt = p.ParserStatement()
	if stmt == nil {
		t.Fatal("ParseStatement() returned nil")
	}

	qstmt, ok = stmt.(*QueryStatement)
	if !ok {
		t.Errorf("s not *QueryStatement. got=%T", qstmt)
	}

	if qstmt.String() != string(lblInput) {
		t.Errorf("Expected %s however got %s\n", string(input), qstmt.String())
	}
	if qstmt.MetricName != "http_requests_total" {
		t.Errorf("Expected MetricName '%s' got '%s'", "http_requests_total", qstmt.MetricName)
	}

	if len(qstmt.Labels) != 2 {
		t.Fatalf("Expected 2 labels got '%+v'", qstmt.Labels)
	}
	if qstmt.Labels[0].Label != "job" && qstmt.Labels[0].Value == "prometheus" {
		t.Fatalf("Expected label to be 'job' and value 'prometheus' job is '%s' and value '%s' ", qstmt.Labels[0].Label, qstmt.Labels[0].Value)
	}
}

type stmtTest struct {
	input     []byte
	expLabels []LabelMatch
	duration  time.Duration
}

func TestQueryMatchStatmeent(t *testing.T) {

	tests := []stmtTest{
		{[]byte(`http_requests_total{job="prometheus",group="canary"}`), []LabelMatch{
			{Label: "job", Op: EqToken, Value: "prometheus"},
			{Label: "group", Op: EqToken, Value: "canary"},
		}, 0},
		{[]byte(`http_requests_total{job!="prometheus",group="canary"}`), []LabelMatch{
			{Label: "job", Op: NotEqToken, Value: "prometheus"},
			{Label: "group", Op: EqToken, Value: "canary"},
		}, 0},
		{[]byte(`http_requests_total{job=~"prometheus",group="canary"}`), []LabelMatch{
			{Label: "job", Op: RegexToken, Value: "prometheus"},
			{Label: "group", Op: EqToken, Value: "canary"},
		}, 0},
		{[]byte(`http_requests_total{job!~"prometheus",group="canary"}`), []LabelMatch{
			{Label: "job", Op: NotRegexToken, Value: "prometheus"},
			{Label: "group", Op: EqToken, Value: "canary"},
		}, 0},
		{[]byte(`http_requests_total{job!~"prometheus",group="canary"}[5m]`), []LabelMatch{
			{Label: "job", Op: NotRegexToken, Value: "prometheus"},
			{Label: "group", Op: EqToken, Value: "canary"},
		}, time.Duration(5 * time.Minute)},
		{[]byte(`http_requests_total{job="prometheus",group="canary"}[10h5m]`), []LabelMatch{
			{Label: "job", Op: EqToken, Value: "prometheus"},
			{Label: "group", Op: EqToken, Value: "canary"},
		}, time.Duration((10 * time.Hour) + (5 * time.Minute))},
	}

	for _, qt := range tests {
		l := Lex(qt.input)
		p := NewParser(l)
		stmt := p.ParserStatement()
		if stmt == nil {
			t.Fatalf("Expected statement '%s' got nil", string(qt.input))
		}

		q, ok := stmt.(*QueryStatement)
		if !ok {
			t.Fatalf("Expected QueryStatement got=%T", q)
		}

		if q.Duration != qt.duration {
			t.Errorf("for query '%s' Expected duration %d got %d", string(qt.input), qt.duration, q.Duration)
		}
		if len(qt.expLabels) != len(q.Labels) {
			t.Fatalf("Expected %d labels got %d", len(qt.expLabels), len(q.Labels))
		}
		for i, tlbl := range qt.expLabels {
			l := q.Labels[i]
			if l.Label != tlbl.Label {
				t.Errorf("Index: %d expected label '%s' got '%s'", i, tlbl.Label, l.Label)
			}

			if l.Op != tlbl.Op {
				t.Errorf("Index: %d expected op '%s' got '%s'", i, tlbl.Op, l.Op)
			}

			if l.Value != tlbl.Value {
				t.Errorf("Index: %d expected value '%s' got '%s'", i, tlbl.Value, l.Value)
			}
		}
	}

}

type aggStmtTest struct {
	stmtTest
	function   string
	by         []string
	past       time.Duration
	resolution time.Duration
}

func TestAggStatmeent(t *testing.T) {

	tests := []aggStmtTest{
		{stmtTest: stmtTest{
			[]byte(`rate(http_requests_total{job="prometheus",group="canary"})`), []LabelMatch{
				{Label: "job", Op: EqToken, Value: "prometheus"},
				{Label: "group", Op: EqToken, Value: "canary"},
			}, 0},
			function: "rate", by: []string{}, past: 0, resolution: 0,
		},
		{stmtTest: stmtTest{
			[]byte(`sum by (job) (http_requests_total{job="prometheus",group="canary"})`), []LabelMatch{
				{Label: "job", Op: EqToken, Value: "prometheus"},
				{Label: "group", Op: EqToken, Value: "canary"},
			}, 0},
			function: "sum", by: []string{"job"}, past: 0, resolution: 0,
		},
		{stmtTest: stmtTest{
			[]byte(`rate(http_requests_total{job="prometheus",group="canary"})[30m:1m]`), []LabelMatch{
				{Label: "job", Op: EqToken, Value: "prometheus"},
				{Label: "group", Op: EqToken, Value: "canary"},
			}, 0},
			function: "rate", by: []string{"job"}, past: 30 * time.Minute, resolution: 1 * time.Minute,
		},
		{stmtTest: stmtTest{
			[]byte(`rate(http_requests_total{job="prometheus",group="canary"}[1m])[30m:1m]`), []LabelMatch{
				{Label: "job", Op: EqToken, Value: "prometheus"},
				{Label: "group", Op: EqToken, Value: "canary"},
			}, 1 * time.Minute},
			function: "rate", by: []string{"job"}, past: 30 * time.Minute, resolution: 1 * time.Minute,
		},
	}

	for _, qt := range tests {
		l := Lex(qt.input)
		p := NewParser(l)
		stmt := p.ParserStatement()
		if stmt == nil {
			t.Fatalf("Expected statement '%s' got nil", string(qt.input))
		}

		a, ok := stmt.(*AccStatement)
		if !ok {
			t.Fatalf("Expected AccStatement got=%T", a)
		}
		if a == nil {
			t.Fatal("AccStatement is nil")
		}

		if a.Query.Duration != qt.duration {
			t.Errorf("for query '%s' Expected duration %d got %d", string(qt.input), qt.duration, a.Query.Duration)
		}
		if len(qt.expLabels) != len(a.Query.Labels) {
			t.Fatalf("Expected %d labels got %d", len(qt.expLabels), len(a.Query.Labels))
		}
		for i, tlbl := range qt.expLabels {
			l := a.Query.Labels[i]
			if l.Label != tlbl.Label {
				t.Errorf("Index: %d expected label '%s' got '%s'", i, tlbl.Label, l.Label)
			}

			if l.Op != tlbl.Op {
				t.Errorf("Index: %d expected op '%s' got '%s'", i, tlbl.Op, l.Op)
			}

			if l.Value != tlbl.Value {
				t.Errorf("Index: %d expected value '%s' got '%s'", i, tlbl.Value, l.Value)
			}
		}

		if a.Function.Literal != qt.function {
			t.Errorf("Expected function '%s' got '%s'", qt.function, a.Function.Literal)
		}

		if a.Past.Nanoseconds() != qt.past.Nanoseconds() {
			t.Errorf("Expected past %v got %v", qt.past, a.Past)
		}
		if a.Resolution.Nanoseconds() != qt.resolution.Nanoseconds() {
			t.Errorf("Expected resolution %v got %v", qt.resolution, a.Resolution)
		}

	}

}
