package prom

import (
	"testing"
)

type lexTest struct {
	expType    TokenType
	expLiteral string
}

func runLexTests(t *testing.T, input []byte, tests []lexTest) {
	l := Lex(input)

	for i, tt := range tests {
		tok := l.NextToken()

		if tok.Type != tt.expType {
			t.Fatalf("tests[%d] - tokentype wrong. expected=%q, got=%q", i, tt.expType, tok.Type)
		}

		if tok.Literal != tt.expLiteral {
			t.Fatalf("tests[%d] - literal wrong. expected=%q, got=%q", i, tt.expLiteral, tok.Literal)
		}
	}

}
func TestDelimiters(t *testing.T) {
	input := []byte(`=+(){},;:`)

	tests := []lexTest{
		{EQ, "="},
		{PLUS, "+"},
		{LPAREN, "("},
		{RPAREN, ")"},
		{LBRACE, "{"},
		{RBRACE, "}"},
		{COMMA, ","},
		{SEMICOLON, ";"},
		{COLON, ":"},
		{EOF, ""},
	}
	runLexTests(t, input, tests)

}

func TestSimpleStatment(t *testing.T) {
	// rate(sndl_httphandler_sndl_http_response_seconds[1m])

	input := []byte("rate(sndl_httphandler_sndl_http_response_seconds[1m])")
	tests := []lexTest{
		{RATE, "rate"},
		{LPAREN, "("},
		{IDENT, "sndl_httphandler_sndl_http_response_seconds"},
		{LBRACKET, "["},
		{INT, "1"},
		{IDENT, "m"},
		{RBRACKET, "]"},
		{RPAREN, ")"},
		{EOF, ""},
	}
	runLexTests(t, input, tests)
}

func TestNotRegexHttp(t *testing.T) {
	// http_requests_total{environment=~"staging|testing|development",method!="GET"}

	input := []byte(`http_requests_total{environment=~"staging|testing|development",method!="GET"}`)
	tests := []lexTest{
		{IDENT, "http_requests_total"},
		{LBRACE, "{"},
		{IDENT, "environment"},
		{REGEX, "=~"},
		{TEXT, "staging|testing|development"},
		{COMMA, ","},
		{IDENT, "method"},
		{NOT_EQ, "!="},
		{TEXT, "GET"},
		{RBRACE, "}"},
		{EOF, ""},
	}
	runLexTests(t, input, tests)
}

func TestModifier(t *testing.T) {
	// sum(http_requests_total{method="GET"} @ 1609746000)
	input := []byte(`sum(http_requests_total{method="GET"} @ 1609746000)`)

	tests := []lexTest{
		{SUM, "sum"},
		{LPAREN, "("},
		{IDENT, "http_requests_total"},
		{LBRACE, "{"},
		{IDENT, "method"},
		{EQ, "="},
		{TEXT, "GET"},
		{RBRACE, "}"},
		{AT, "@"},
		{INT, "1609746000"},
		{RPAREN, ")"},
		{EOF, ""},
	}
	runLexTests(t, input, tests)
}
