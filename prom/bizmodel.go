package prom

import (
	"context"
	"fmt"
	"log"
	"time"

	lua "github.com/yuin/gopher-lua"
)

type Window struct {
	start time.Time
	end   time.Time
}

func NewWindow(start, end time.Time) Window {
	return Window{start: start, end: end}
}

func QueryAndLoadState(L *lua.LState, ctx context.Context, client Client, metrics []string, pre, post Window, window string) (map[string]*lua.LTable, error) {

	data := make(map[string]*lua.LTable)
	for _, m := range metrics {
		pre, err := client.RangeQuery(QueryStatement{MetricName: m}, pre.start, pre.end, window)
		if err != nil {
			log.Printf("Error querying metrics %s\n", err)
			continue
			//return nil, err
		}

		tbl := L.NewTable()
		preAcc := AccumulationFromData(pre)
		preUd := L.NewUserData()
		preUd.Value = Accumulation(preAcc)
		L.SetMetatable(preUd, L.GetTypeMetatable(AccumulationTypeName))
		tbl.RawSetString("pre", preUd)
		post, err := client.RangeQuery(QueryStatement{MetricName: m}, post.start, post.end, window)
		if err != nil {
			log.Printf("Error querying metrics %s\n", err)
			continue
			//return nil, err
		}
		postAcc := &simpleAcc{basicAcc{data: post}}
		fmt.Printf("%s: Post Acc Sum is %0.2f\n", m, postAcc.Sum())
		postUd := L.NewUserData()
		postUd.Value = Accumulation(postAcc)
		L.SetMetatable(postUd, L.GetTypeMetatable(AccumulationTypeName))
		tbl.RawSetString("post", postUd)

		// Do the diffs
		diff := basicAcc{Diff(preAcc.Data(), postAcc.Data())}
		diffData := &simpleAcc{basicAcc{data: diff.SampleDeltas()}}
		fmt.Printf("%s Diff sum is %0.2f\n", m, diffData.Sum())
		diffUd := L.NewUserData()
		diffUd.Value = Accumulation(diffData)
		L.SetMetatable(diffUd, L.GetTypeMetatable(AccumulationTypeName))
		tbl.RawSetString("diff", diffUd)

		data[m] = tbl

	}
	return data, nil
}
func LoadChangeData(L *lua.LState, ctx context.Context, client Client, cfg *PromCfg, pre, post Window) (map[string]bool, error) {
	waitTime := time.Until(post.end)
	fmt.Printf("Waiting %+v\n", waitTime)
	if waitTime > 0 {
		time.Sleep(waitTime)
	}
	w := "5m"
	if cfg.Window != "" {
		w = cfg.Window
	}
	data, err := QueryAndLoadState(L, ctx, client, cfg.Metrics, pre, post, w)
	if err != nil {
		return nil, err
	}
	for m, tbl := range data {
		L.SetGlobal(m, tbl)
	}
	// Evaluate the posts
	expResp := map[string]bool{}
	for _, exp := range cfg.Expect {
		err := L.DoString("return " + exp)
		// TODO: Figure out something better to do with error than print
		if err != nil {
			log.Printf("Error '%s 'running expectation  `%s` \n", err, exp)
			expResp[exp] = false
		} else {
			resp := L.Get(-1)
			expResp[exp] = lua.LVAsBool(resp)
		}
		// TODO: Need to figure out what to do with failed expectations
		if len(expResp) > 0 {
			for k, v := range expResp {
				fmt.Printf("Expect: Failed '%s' with value '%+v'\n", k, v)
			}
		}
	}
	return expResp, nil
}
func LoadData(L *lua.LState, ctx context.Context, client Client, cfg *PromCfg) (map[string]bool, error) {

	st := "1m"
	if cfg.Step != "" {
		st = cfg.Step
	}
	w := "10m"
	if cfg.Window != "" {
		w = cfg.Window
	}

	window, err := time.ParseDuration(w)
	if err != nil {
		return nil, err
	}

	end := time.Now()
	start := end.Add(-window)
	for _, m := range cfg.Metrics {
		d, err := client.RangeQuery(QueryStatement{MetricName: m}, start, end, st)
		if err != nil {
			log.Printf("Error querying metrics %s\n", err)
			return nil, err
		}

		tbl := L.NewTable()

		acc := &simpleAcc{basicAcc{data: d}}
		accUd := L.NewUserData()
		accUd.Value = Accumulation(acc)
		L.SetMetatable(accUd, L.GetTypeMetatable(AccumulationTypeName))
		tbl.RawSetString("data", accUd)

		deltaData := &simpleAcc{basicAcc{data: acc.SampleDeltas()}}
		dataUd := L.NewUserData()
		dataUd.Value = Accumulation(deltaData)
		L.SetMetatable(dataUd, L.GetTypeMetatable(AccumulationTypeName))
		tbl.RawSetString("diff", dataUd)

		L.SetGlobal(m, tbl)
	}
	// Evaluate the posts
	expResp := map[string]bool{}
	for _, exp := range cfg.Expect {
		err := L.DoString("return " + exp)
		// TODO: Figure out something better to do with error than print
		if err != nil {
			log.Printf("Error '%s 'running expectation  `%s` \n", err, exp)
			expResp[exp] = false
		} else {
			resp := L.Get(-1)
			expResp[exp] = lua.LVAsBool(resp)
		}
	}
	return expResp, nil
}
