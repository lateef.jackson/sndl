package prom

import (
	"sort"
	"time"

	"github.com/prometheus/common/model"
	lua "github.com/yuin/gopher-lua"
)

type Sample struct {
	Timestamp time.Time
	Value     float64
}

// Interface that defines a set of useful functions a signal
type Accumulation interface {
	Data() map[string][]Sample
	// Stats
	SampleSize() int
	Sum() float64
	Average() float64
	Median() float64
	Min() float64
	Max() float64
	// Time functions
	TimeRange() (float64, float64)
	Timestamp() float64
	// TODO: Figure out how to capture absence of a signal
}
type RateOfChange interface {
	Accumulation
}

func Diff(a, b MetricData) MetricData {

	diff := map[string][]Sample{}
	for an, aSmpls := range a {
		diffSmpls := []Sample{}
		bSmpls := b[an]
		for i, av := range aSmpls {
			if i < len(bSmpls) {
				bv := bSmpls[i]
				dv := bv.Value - av.Value
				diffSmpls = append(diffSmpls, Sample{Timestamp: bv.Timestamp, Value: dv})
			} else if i > len(bSmpls) {
				diffSmpls = append(diffSmpls, av)
			}
		}
		if len(bSmpls) > len(aSmpls) {
			diffSmpls = append(diffSmpls, bSmpls[len(aSmpls):]...)
		}
		diff[an] = diffSmpls
	}
	return diff
}

type basicAcc struct {
	data map[string][]Sample
}

func (ba *basicAcc) Data() map[string][]Sample {
	return ba.data
}
func (ba *basicAcc) SampleSize() int {
	samples := 0
	for _, m := range ba.data {
		for range m {
			samples += 1
		}
	}
	return samples
}

func (ba *basicAcc) Sum() float64 {
	t := float64(0)
	for _, m := range ba.data {
		for _, v := range m {
			t += v.Value
		}
	}
	return t
}
func (ba *basicAcc) allTimestamps() []float64 {
	allTime := []float64{}
	for _, m := range ba.data {
		for _, v := range m {
			// Convert to seconds and append
			allTime = append(allTime, float64(v.Timestamp.UnixMicro())/1000000)
		}
	}
	return allTime
}

// Just for sorting arrays
type secondArr []float64

func (a secondArr) Len() int           { return len(a) }
func (a secondArr) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a secondArr) Less(i, j int) bool { return a[i] < a[j] }

func (ba *basicAcc) TimeRange() (float64, float64) {
	if len(ba.allTimestamps()) == 0 {
		return 0, 0
	}
	allTime := secondArr(ba.allTimestamps())
	sort.Sort(allTime)
	return float64(allTime[0]), float64(allTime[len(allTime)-1])
}

func (ba *basicAcc) Timestamp() float64 {
	allTime := secondArr(ba.allTimestamps())
	if len(allTime) < 1 {
		return 0
	}
	sort.Sort(allTime)
	return allTime[len(allTime)-1]
}

func (s *basicAcc) SampleDeltas() map[string][]Sample {

	deltas := map[string][]Sample{}
	for name, m := range s.data {
		dtl := []Sample{}
		first := true
		prevTime := int64(0)
		prevValue := float64(0)
		for _, v := range m {
			if first {
				first = false
			} else {
				dtl = append(dtl, Sample{
					Timestamp: time.Unix(v.Timestamp.Unix()-prevTime, 0),
					Value:     v.Value - prevValue,
				})
			}
			prevTime = v.Timestamp.Unix()
			prevValue = v.Value
		}
		deltas[name] = dtl
	}
	return deltas
}

type simpleAcc struct {
	basicAcc
}

func (s *simpleAcc) Average() float64 {
	return s.Sum() / float64(s.SampleSize())
}

func (s *simpleAcc) Median() float64 {
	vls := []float64{}
	for _, m := range s.data {
		for _, v := range m {
			vls = append(vls, v.Value)
		}
	}

	l := len(vls)
	if l == 0 { // TODO: Is this a useful return?
		return 0
	} else if l%2 == 0 { // Even so average middle
		return (vls[l/2-1] + vls[l/2]) / 2
	}
	// Take middle value
	return vls[l/2]
}
func (ba *basicAcc) allValues() []float64 {
	all := []float64{}
	for _, m := range ba.data {
		for _, v := range m {
			all = append(all, v.Value)
		}
	}
	return all
}

func (ba *basicAcc) Min() float64 {

	all := ba.allValues()
	m := all[0]
	for _, x := range all {
		if m > x {
			m = x
		}
	}
	return m
}
func (ba *basicAcc) Max() float64 {

	all := ba.allValues()
	m := float64(0)
	if len(all) > 1 {
		m = all[0]
		for _, x := range all {
			if m < x {
				m = x
			}
		}
	}
	return m
}

type rateOfChangeAcc struct {
	basicAcc
}

/*
func (roc *rateOfChangeAcc) Average() float64 {
	if len(roc.data) < 2 {
		return 0
	}

	//pre := ,l
}*/

func checkAccumulation(L *lua.LState) Accumulation {
	ud := L.CheckUserData(1)
	if v, ok := ud.Value.(Accumulation); ok {
		return v
	}
	L.ArgError(1, "accumulator not found")
	return nil
}

func accData(L *lua.LState) int {
	acc := checkAccumulation(L)
	if acc == nil {
		L.Push(lua.LNil)
		return 1
	}

	tbl := L.NewTable()
	for k, v := range acc.Data() {
		nt := L.NewTable()
		for i, s := range v {
			stb := L.NewTable()
			stb.RawSetString("timestamp", lua.LNumber(s.Timestamp.Unix()))
			stb.RawSetString("value", lua.LNumber(s.Value))
			nt.RawSetInt(i+1, stb)
		}
		tbl.RawSetString(k, nt)
	}
	L.Push(tbl)
	return 1
}

func accSampleSize(L *lua.LState) int {
	acc := checkAccumulation(L)
	if acc == nil {
		L.Push(lua.LNil)
		return 1
	}
	L.Push(lua.LNumber(acc.SampleSize()))
	return 1
}
func accSum(L *lua.LState) int {
	acc := checkAccumulation(L)
	if acc == nil {
		L.Push(lua.LNil)
		return 1
	}

	L.Push(lua.LNumber(acc.Sum()))
	return 1
}

func accAverage(L *lua.LState) int {
	acc := checkAccumulation(L)
	if acc == nil {
		L.Push(lua.LNil)
		return 1
	}
	L.Push(lua.LNumber(acc.Average()))
	return 1
}
func accMedian(L *lua.LState) int {
	acc := checkAccumulation(L)
	if acc == nil {
		L.Push(lua.LNil)
		return 1
	}
	L.Push(lua.LNumber(acc.Median()))
	return 1
}
func accMin(L *lua.LState) int {
	acc := checkAccumulation(L)
	if acc == nil {
		L.Push(lua.LNil)
		return 1
	}
	L.Push(lua.LNumber(acc.Min()))
	return 1
}
func accMax(L *lua.LState) int {
	acc := checkAccumulation(L)
	if acc == nil {
		L.Push(lua.LNil)
		return 1
	}
	L.Push(lua.LNumber(acc.Max()))
	return 1
}

func accTimeRange(L *lua.LState) int {
	acc := checkAccumulation(L)
	if acc == nil {
		L.Push(lua.LNil)
		return 1
	}
	start, end := acc.TimeRange()
	L.Push(lua.LNumber(start))
	L.Push(lua.LNumber(end))
	return 2
}

func accTimestamp(L *lua.LState) int {
	acc := checkAccumulation(L)
	if acc == nil {
		L.Push(lua.LNil)
		return 1
	}
	L.Push(lua.LNumber(acc.Timestamp()))
	return 1
}

type timeSeries struct {
	data model.Matrix
}

func checkTimeSeries(L *lua.LState) *timeSeries {
	ud := L.CheckUserData(1)
	if v, ok := ud.Value.(*timeSeries); ok {
		return v
	}
	L.ArgError(1, "time series not found")
	return nil
}

func matrixToMap(mtx model.Matrix) map[string][]Sample {
	smpls := map[string][]Sample{}
	//fmt.Printf("Nodes are %d\n", len(mtx))
	if len(mtx) == 0 {
		return smpls
	}
	for _, m := range mtx {
		s := []Sample{}
		for _, v := range m.Values {
			s = append(s, Sample{
				Timestamp: time.Unix(0, int64(v.Timestamp)*int64(time.Millisecond)),
				Value:     float64(v.Value),
			})
		}
		smpls[m.Metric.String()] = s
	}
	return smpls
}

func AccumulationFromData(data MetricData) Accumulation {
	return Accumulation(&simpleAcc{basicAcc{data: data}})
}

func AccumulationDeltasFromMatrix(mtx model.Matrix) Accumulation {
	bsc := simpleAcc{basicAcc: basicAcc{matrixToMap(mtx)}}
	return Accumulation(&simpleAcc{basicAcc{data: bsc.SampleDeltas()}})
}

func basic(L *lua.LState) int {
	ts := checkTimeSeries(L)

	if ts == nil {
		L.Push(lua.LNil)
		return 1
	}

	smpls := matrixToMap(ts.data)
	acc := &simpleAcc{basicAcc{data: smpls}}
	ud := L.NewUserData()
	ud.Value = Accumulation(acc)
	L.SetMetatable(ud, L.GetTypeMetatable(AccumulationTypeName))
	L.Push(ud)
	return 1
}

func delta(L *lua.LState) int {

	ts := checkTimeSeries(L)

	if ts == nil {
		L.Push(lua.LNil)
		return 1
	}

	smpls := matrixToMap(ts.data)
	bsc := basicAcc{data: smpls}
	// Use Delta as the base data
	acc := &simpleAcc{basicAcc{data: bsc.SampleDeltas()}}
	ud := L.NewUserData()
	ud.Value = Accumulation(acc)
	L.SetMetatable(ud, L.GetTypeMetatable(AccumulationTypeName))
	L.Push(ud)
	return 1
}

var timeSeriesFuncs = map[string]lua.LGFunction{
	"basic": basic,
	"delta": delta,
	//rate_of_change
}
var accumulationFuncs = map[string]lua.LGFunction{
	"data":        accData,
	"sample_size": accSampleSize,
	"sum":         accSum,
	"average":     accAverage,
	"median":      accMedian,
	"min":         accMin,
	"max":         accMax,
	"time_range":  accTimeRange,
	"timestamp":   accTimestamp,
}
