package prom

import (
	"bytes"
	"strings"
	"time"
)

type Node interface {
	TokenLiteral() string
	String() string
}

type Statement interface {
	Node
	statementNode()
}

type Expression interface {
	Node
	expressionNode()
}

type NumberLiteral struct {
	Token Token
	Value float64
}

func (nl *NumberLiteral) expressionNode()      {}
func (nl *NumberLiteral) TokenLiteral() string { return nl.Token.Literal }
func (nl *NumberLiteral) String() string       { return nl.Token.Literal }

type TextLiteral struct {
	Token Token
	Value string
}

func (tl *TextLiteral) expressionNode()      {}
func (tl *TextLiteral) TokenLiteral() string { return tl.Token.Literal }
func (tl *TextLiteral) String() string       { return tl.Token.Literal }

type LabelMatch struct {
	Label string
	Value string
	Op    Token
}

type Range struct {
	Start time.Time
	End   time.Time
}

type QueryStatement struct {
	MetricName string
	Labels     []LabelMatch
	Duration   time.Duration
	Offset     time.Duration
}

func (qs *QueryStatement) statementNode()       {}
func (qs *QueryStatement) TokenLiteral() string { return qs.MetricName }

func (qs *QueryStatement) String() string {
	var out bytes.Buffer

	out.WriteString(qs.MetricName)
	if len(qs.Labels) > 0 {
		out.WriteString(LBRACE)
		lbs := []string{}
		for _, l := range qs.Labels {
			lbs = append(lbs, l.Label+l.Op.Literal+"\""+l.Value+"\"")
		}
		out.WriteString(strings.Join(lbs, ","))
		out.WriteString(RBRACE)
	}
	if qs.Duration.Microseconds() > 0 {
		out.WriteString(LBRACKET)
		out.WriteString(qs.Duration.String())
		out.WriteString(RBRACKET)
	}
	if qs.Offset.Microseconds() > 0 {
		out.WriteString(" offset ")
		out.WriteString(qs.Offset.String())
	}

	return out.String()
}

type AccStatement struct {
	Function   Token
	Query      QueryStatement
	Param      Expression
	By         []string
	Past       time.Duration
	Resolution time.Duration
}

func (ac *AccStatement) statementNode()       {}
func (ac *AccStatement) TokenLiteral() string { return ac.Function.Literal }
func (ac *AccStatement) String() string {
	var out bytes.Buffer
	out.WriteString(ac.Function.Literal)
	if ac.Param != nil {
		out.WriteString(LPAREN)
		out.WriteString(ac.Param.String())
		out.WriteString(RPAREN)
	}
	if len(ac.By) > 0 {
		out.WriteString(" ")
		out.WriteString(BY)
		out.WriteString(" ")
		out.WriteString(LPAREN)
		out.WriteString(strings.Join(ac.By, COMMA+" "))
		out.WriteString(RPAREN)
		out.WriteString(" ")
	}
	out.WriteString(LPAREN)
	out.WriteString(ac.Query.String())
	if ac.Past.Nanoseconds() > 0 || ac.Resolution.Nanoseconds() > 0 {

		out.WriteString(LBRACKET)
		if ac.Past.Nanoseconds() > 0 {
			out.WriteString(ac.Past.String())
		}
		if ac.Resolution.Nanoseconds() > 0 {
			out.WriteString(":")
			out.WriteString(ac.Resolution.String())
		}
		out.WriteString(RBRACKET)
	}
	out.WriteString(RPAREN)
	return out.String()
}
