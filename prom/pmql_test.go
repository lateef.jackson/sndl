package prom

import (
	"context"
	"testing"
	"time"

	lua "github.com/yuin/gopher-lua"
)

// Useful for writing test outside the package
type MockPromClient struct {
	Data map[string]MetricData
	Err  error
}

func (mpc *MockPromClient) RangeQuery(qs QueryStatement, start time.Time, end time.Time, step string) (MetricData, error) {
	md, ok := mpc.Data[qs.MetricName]
	if !ok {
		return nil, nil
	}
	return md, mpc.Err
}

// TODO: Parse yml string and run simple expressions
var testConcptYml = `
---
type: "window"
metrics: # Business Model KPI's
- "login"
- "transaction"
window: "10m" # Given a 10 minute window
expect: # Set of expectations required for successful business model
- "login.diff:sum() > 100" # After any change the set of logins is greater than 100 
- "transaction.diff:sum() > 10" # After any change there are more than 10 transactions
- "login.data:max() > 100" # After any change the set of logins is greater than 100 
- "transaction.data:max() > 10" # After any change there are more than 10 transactions
`

func TestConfigParse(t *testing.T) {
	cfg, err := parsePromChangeCfg([]byte(testConcptYml))
	if err != nil {
		t.Fatal(err)
	}
	if len(cfg.Metrics) != 2 {
		t.Fatalf("Expected 2 metrics however there are: %d", len(cfg.Metrics))
	}
	if cfg.Metrics[0] != "login" {
		t.Fatalf("first metrics should be 'login' however it is '%s'", cfg.Metrics[0])
	}
	if cfg.Metrics[1] != "transaction" {
		t.Fatalf("first metrics should be 'transaction' however it is '%s'", cfg.Metrics[1])
	}

	if cfg.Window != "10m" {
		t.Fatalf("Expected window to be '10m' however it is '%s'", cfg.Window)
	}

	if len(cfg.Expect) != 4 {
		t.Fatalf("Expect length of 4 however got %d", len(cfg.Expect))
	}

	if cfg.Expect[0] != "login.diff:sum() > 100" {
		t.Fatalf("First expect should  'login.diff:sum() > 100' however it is '%s'", cfg.Expect[0])
	}

	if cfg.Expect[1] != "transaction.diff:sum() > 10" {
		t.Fatalf("Second expect should 'transaction.diff:sum() > 10' however it is '%s'", cfg.Expect[1])
	}
}

func TestConceptModel(t *testing.T) {

	cfg, err := parsePromChangeCfg([]byte(testConcptYml))
	if err != nil {
		t.Fatal(err)
	}
	mpc := &MockPromClient{
		Data: map[string]MetricData{
			cfg.Metrics[0]: {
				"host1": {
					{
						Timestamp: time.Time{}.Add(1 * time.Second),
						Value:     0,
					},
					{
						Timestamp: time.Time{}.Add(1 * time.Second),
						Value:     101,
					},
				}},
			cfg.Metrics[1]: {
				"host1": {
					{
						Timestamp: time.Time{}.Add(1 * time.Second),
						Value:     0,
					},
					{
						Timestamp: time.Time{}.Add(1 * time.Second),
						Value:     11,
					},
				}},
		},
		Err: nil,
	}

	ctx := context.Background()
	L := lua.NewState(lua.Options{})
	OpenProm(L)
	exps, err := LoadData(L, ctx, mpc, cfg)
	if err != nil {
		t.Fatal(err)
	}
	if len(exps) != 4 {
		t.Fatalf("Expected 4 expects got %d", len(exps))
	}
	for n, v := range exps {
		if !v {
			t.Errorf("Exp failed for '%s' value %+v", n, v)
		}
	}
	L.Close()
	// Test false expect
	mpc.Data["login"]["host1"][1].Value = 10
	mpc.Data["transaction"]["host1"][1].Value = 1
	L = lua.NewState(lua.Options{})
	OpenProm(L)
	exps, err = LoadData(L, ctx, mpc, cfg)
	if err != nil {
		t.Fatal(err)
	}
	for n, v := range exps {
		if v {
			t.Errorf("Exp should have failed but did not for '%s' value %+v", n, v)
		}
	}
}
