package prom

import "testing"

func TestIsAccumulationFunc(t *testing.T) {
	if !IsAccumulationFunc("rate") {
		t.Fatal("rate should be accumulation function")
	}
	if IsAccumulationFunc("foobar") {
		t.Fatal("'foobar' is not an accumulation function")
	}
}
