package lstate

import (
	"context"

	lua "github.com/yuin/gopher-lua"
	"gitlab.com/lateef.jackson/sndl"
	"gitlab.com/lateef.jackson/sndl/chat"
	"gitlab.com/lateef.jackson/sndl/delta"
	"gitlab.com/lateef.jackson/sndl/prom"
	sqlite "gitlab.com/lateef.jackson/sndl/sqlitesndl"
)

var (
	defaultLuaState = []struct {
		name string
		fn   lua.LGFunction
	}{
		{lua.LoadLibName, lua.OpenPackage}, // Must be first
		{lua.BaseLibName, sndl.OpenBuiltins},
		{lua.StringLibName, lua.OpenString},
		{lua.IoLibName, sndl.OpenIO},
		{lua.OsLibName, sndl.OpenOs},
		{chat.ChatLibName, chat.OpenChat},
		{sqlite.Sqlite3Module, sqlite.OpenSqlite3},
		{prom.PromLibName, prom.OpenProm},
		{sndl.StacheLibName, sndl.OpenStache},
		{delta.DeltaLibName, delta.OpenDelta},
		{lua.TabLibName, lua.OpenTable},
		{lua.MathLibName, lua.OpenMath},
	}
)

func BuildDefaultLuaState(ctx context.Context) *lua.LState {
	//fmt.Printf("Callerm Package in context is %s\n", ctx.Value(sndl.CallerModuleKey))
	L := lua.NewState(lua.Options{SkipOpenLibs: true})
	L.SetContext(ctx)
	for _, pair := range defaultLuaState {
		if err := L.CallByParam(lua.P{
			Fn:      L.NewFunction(pair.fn),
			NRet:    0,
			Protect: true,
		}, lua.LString(pair.name)); err != nil {
			panic(err)
		}
	}
	L.PreloadModule("socket", sndl.OpenSecureSocket)
	L.PreloadModule("http", sndl.OpenHttp)
	return L
}
