package sndl

import (
	lua "github.com/yuin/gopher-lua"
)

const CallerModuleKey = "callerModule"
const configKey = "sndlConfig"

func getConfig(L *lua.LState) *Config {
	ctx := L.Context()
	v := ctx.Value(configKey)
	if v != nil {
		return v.(*Config)
	}
	return nil
}

func getCallingPackage(L *lua.LState) string {
	ctx := L.Context()
	if ctx.Value(CallerModuleKey) != nil {
		return ctx.Value(CallerModuleKey).(string)
	}
	return ""
}
